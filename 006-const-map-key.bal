import ballerina/io;
const SYMBOL_A = "*";
enum Symbols {
    SYMBOL_B = "#",
    SYMBOL_C = "%"
}
public function main() {
    final string SYMBOL_D = "§";
    { // prints: {"SYMBOL_A":0,"SYMBOL_B":0,"SYMBOL_C":0,"SYMBOL_D":0}
        map<int> symbolCounts = { SYMBOL_A: 0, SYMBOL_B: 0, SYMBOL_C: 0, SYMBOL_D: 0 };
        io:println(symbolCounts);
    }
    { // prints: {"*":0,"#":0,"%":0,"§":0}
        map<int> symbolCounts = { [SYMBOL_A]: 0, [SYMBOL_B]: 0, [SYMBOL_C]: 0, [SYMBOL_D]: 0 };
        io:println(symbolCounts);
    }
    { // prints: {"*":0,"#":0,"%":0,"§":0}
        map<int> symbolCounts = {};
        symbolCounts[SYMBOL_A] = 0;
        symbolCounts[SYMBOL_B] = 0;
        symbolCounts[SYMBOL_C] = 0;
        symbolCounts[SYMBOL_D] = 0;
        io:println(symbolCounts);
    }

    // Symbols is a type (enum is a type as it just a syntax sugar)
    if "#" is Symbols {
        io:println("# is a symbol");
    }
    if "&" !is Symbols {
        io:println("& is not a symbol");
    }
    // SYMBOL_A is a type
    if "*" is SYMBOL_A {
        io:println("* is a symbol");
    }
    // ERROR: unknown type 'SYMBOL_D'
    // if "&" is SYMBOL_D {
    //     io:println("& is a symbol");
    // }
}
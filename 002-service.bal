import ballerina/http;
import ballerina/log;

service /hello on new http:Listener(8080) {
    resource function get . () returns string {
        return "Hello World!";
    }

    // curl -v http://localhost:8080/hello \
    // --header 'Content-Type: text/xml' \
    // --data-binary '<whatever><name>Jani</name><foo>Alice</foo><name>Gary</name></whatever>'
    resource function post . (@http:Payload xml payload) returns string {
        log:printInfo("received", payload = payload);

        // xml navigation selects all name-elements from anywhere in payload
        xml names = payload/**/<name>;

        // convert name elements to output strings
        string[] outputs = 
            from xml name in names
            where name is xml:Element
            let string nameStr = name.data()
            select string`Hello ${nameStr}!`;

        return string:'join("\n", ...outputs.sort());
    }
}
import ballerina/io;

type Foo record {|
    readonly int id;
    string text;
|};
public function main() {
    Foo[] fooArray = [
        {id: 1, text: "one"},
        {id: 2, text: "two"},
        {id: 42, text: "forty-two"}
    ];

    io:println(fooArray[2]); // by index

    // to table
    table<Foo> key(id) fooTable = table key(id) from Foo f in fooArray select f;
    io:println(fooTable[42]); // by key (int)

    // to map
    map<Foo>|error fooMap1 = map from Foo f in fooArray select [f.id.toString(), f];
    if fooMap1 is map<Foo> {
        io:println(fooMap1["42"]); // by key (string)
    }

    map<Foo> fooMap2 = {};
    error? e = from Foo f in fooArray do {
        string key = f.id.toString();
        fooMap2[key] = f;
    };

    if e is error {
        // it's fine
    }

    io:println(fooMap2["42"]); // by key (string)
}
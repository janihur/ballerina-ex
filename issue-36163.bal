// https://github.com/ballerina-platform/ballerina-lang/issues/36163
import ballerina/io;

public function main() {
    xml x = xml`<_/>`;
    io:println(x);
}
import ballerina/io;

// "supported" input encodings:
// UTF-8
// ISO-8859-15
// windows-1252

public function main(string filePath, string inputEncoding, string? outputEncoding) returns error? {
    io:ReadableByteChannel byteChannel = check io:openReadableFile(filePath);
    io:ReadableCharacterChannel characterChannel = new (byteChannel, inputEncoding);
    string content = check characterChannel.readString();

    if outputEncoding != () {
        io:WritableByteChannel outByteChannel = check io:openWritableFile("/tmp/bal-022.txt");
        io:WritableCharacterChannel outCharacterChannel = new (outByteChannel, outputEncoding);
        _ = check outCharacterChannel.write(content, 0);
    } else {
        io:println(content);
    }
}
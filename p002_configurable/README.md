P002_CONFIGURABLE
=================

https://ballerina.io/learn/configure-ballerina-programs/

Generate 32 bytes length key:

export KEY=$(pwgen 32 1)
echo $KEY 
rahGhee8shu2airo1feexeoSh1IefaiS

That is the key used in this example.

Create a secret file that is not saved to version control:
```
$ cat <<EOF > modules/foo/Config-Secret.toml
[p002_configurable.foo]
secret = "nBSia5gMFAJuTEJlHGIalfHj/g/QOZgxyY1Tj//7bg4W7YiBfRKMRv5HvinVaXbb"
EOF
```

How to run:
```
export BAL_CONFIG_FILES=modules/foo/Config.toml:modules/foo/Config-Secret.toml
bal run -- -Cp002_configurable.foo.key=${KEY}
```

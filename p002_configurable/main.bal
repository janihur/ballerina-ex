import p002_configurable.foo;
import ballerina/io;

public function main() {
    io:print("foo:staticConfig(): ");
    io:println(foo:staticConfig());
    io:print("foo:dynamicConfig(): ");
    io:println(foo:dynamicConfig());
    // foo:foo();
}

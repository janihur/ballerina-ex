import ballerina/crypto;
import ballerina/lang.array;

type Config record {|
    int number;
    string text;
    string optional?;
|};

Config staticConfig_ = {
    number: 42,
    text: "foo from static config"
};

configurable Config config = ?;
configurable string key = ?;
configurable string secret = ?;

public function staticConfig() returns string {
    return staticConfig_.toString();
}

public function dynamicConfig() returns string {
    string decrypted = checkpanic decrypt(secret, key);
    return string`(config ${config.toString()})("encrypted secret" "${secret}")("decrypted secret" "${decrypted}")`;
}

// public function foo() {
//         string encryptedText = checkpanic encrypt(secret, key);
//         io:println(string`encrypted: ${encryptedText}`);
//         io:println(string`decrypted: ${checkpanic decrypt(encryptedText, key)}`);
// }

// function encrypt(string secret, string key) returns string|error {
//     byte[] secretBytes = secret.toBytes();
//     byte[] keyBytes = key.toBytes();

//     byte[] cipherText = check crypto:encryptAesEcb(secretBytes, keyBytes);
    
//     return array:toBase64(cipherText);
// }

function decrypt(string secret, string key) returns string|error {
    byte[] cipherText = check array:fromBase64(secret);
    byte[] keyBytes = key.toBytes();

    byte[] plainText = check crypto:decryptAesEcb(cipherText, keyBytes);

    return check string:fromCodePointInts(plainText);
}
import ballerina/io;

type Foo record {|
    readonly int id;
    string data;
|};
type FooTable table<Foo> key(id);

type Bar record {|
    readonly int id;
    string data;
|};
type BarTable table<Bar> key(id);

type FooBarAssociation record {|
    readonly int fooId;
    readonly int barId;
|};
type FooBarAssociations table<FooBarAssociation> key(fooId, barId);

public function main() {
    FooTable foos = table [
        {id: 1, data: "foo 1"},
        {id: 2, data: "foo 3"},
        {id: 3, data: "foo 3"},
        {id: 4, data: "foo 4"},
        {id: 5, data: "foo 5"}
    ];

    BarTable bars = table [
        {id: 1, data: "bar 1"},
        {id: 2, data: "bar 2"},
        {id: 3, data: "bar 3"},
        {id: 4, data: "bar 4"},
        {id: 5, data: "bar 5"},
        {id: 6, data: "bar 6"}
    ];

    FooBarAssociations fooBarAssociations = table [
        {fooId: 1, barId: 2},
        {fooId: 1, barId: 4},
        {fooId: 3, barId: 4},
        {fooId: 3, barId: 6},
        {fooId: 5, barId: 6}
    ];

    do {
        record {|int fooId; int barId; string data;|}[] fooBars =
            from var relation in fooBarAssociations
            join var foo in foos on relation.fooId equals foo.id
            join var bar in bars on relation.barId equals bar.id
            select {
                fooId: relation.fooId,
                barId: relation.barId,
                data: string`${foo.data};${bar.data}`
            }
        ;

        io:println(fooBars);
    }

    do {
        // TODO pending question in Ballerina Discord
        record {|int fooId; int|() barId; string data;|}[] fooBars =
            from var foo in foos
            outer join var relation in fooBarAssociations on foo.id equals relation.fooId
            select {
                fooId: foo.id,
                barId: relation.barId,
                data: string`${foo.data};`
            };


        io:println(fooBars);
    }
}
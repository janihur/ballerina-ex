import ballerina/io;
public function main() {
    final int a = 1;
    io:println(string`(a ${a})`);

    final int? b = ();
    if b is int {
        io:println(string`(b ${b})`);
    } else {
        io:println("(b nil)");
    }
    
    int|string c = "three";
    if c is string {
        io:println(string`(c ${c})`);
    }
    if c !is int {
        io:println(string`(c ${c})`);
    }

    c = 3;
    if c is int {
        io:println(string`(c ${c})`);
    }
    if c !is string {
        io:println(string`(c ${c})`);
    }

    final D d = {
        d: 4
    };
    if d.d is int {
        io:println(string`(d.d ${<int>d.d})`);
    }
}

type D record {|
    int? d;
|};
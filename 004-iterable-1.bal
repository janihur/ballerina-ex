import ballerina/io;

public type Result record {|
    int value;
|};

public class InclusiveIter {
    *object:Iterable;
    int c;
    final int max;

    function init(int max) {
        self.max = max;
        self.c = 0;
    }

    // This method should return an object that have a `next` method
    public function iterator() returns InclusiveIter {
        return self;
    }

    // Implements the `next` method
    public function next() returns Result? {
        if self.c > self.max {
            self.c = 0; // reset so can be iterated multiple times
            return ();
        }
        Result r = {value: self.c};
        self.c += 1;
        return r;
    }
}

public function main() {
    InclusiveIter iter = new InclusiveIter(10);

    foreach var i in iter {
        io:print(i);
        io:print(" ");
    }
    io:println();

    foreach var i in iter {
        io:print(i);
        io:print(" ");
    }
    io:println();

    // query expression
    final int[] arr = from var i in iter select i; 
    io:println(arr);
}
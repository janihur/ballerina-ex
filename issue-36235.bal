// https://github.com/ballerina-platform/ballerina-lang/issues/36235
import ballerina/http;
import ballerina/io;
import ballerina/xmldata;

public function main() returns error? {
    {
        // CASE 1

        json json1 = { 
            code: "ERROR_CODE", 
            details: "ERROR_DETAILS"
        };

        xml? xml1 = check xmldata:fromJson(json1);

        if xml1 is xml {
            io:println("xml1 is xml");
        }
        io:println(xml1);
    }
    {
        // CASE 2

        http:BadRequest badRequest = { 
            body: { 
                code: "ERROR_CODE", 
                details: "ERROR_DETAILS"
            }
        };
        io:println(badRequest);
        
        json json2 = <json> badRequest?.body;
        xml? xml2 = check xmldata:fromJson(json2);

        if xml2 is xml {
            io:println("xml2 is xml");
        }
        io:println(xml2);
    }
}
// https://github.com/ballerina-platform/ballerina-library/issues/6807
import ballerina/io;
import ballerinax/health.hl7v2;
import ballerinax/health.hl7v23;

string[] EXAMPLE_ARR = [
    string `MSH|^~\&|SENDING_APPLICATION|SENDING_FACILITY|RECEIVING_APPLICATION|RECEIVING_FACITILITY|20240718115723||ORM^O01|12345|P|2.3||||AL||8859/1`,
    string `PID|1|040404-0404^^^^SSN|||Lastname^Givenname^Middlename`,
    string `PV1|1||2039^Room 3^Bed 4^IMW||||||||||||||||||||||||||||||||||||9876^InternalMedicalWard|||||||||||1.2345.777.888`,
    string `PV2|1|||||||||||||||||1011|`,
    string `ORC|NW|20240718-1^FOO|||||^^^^^A||202407181157|||030303-0303^Smith^John^^^^^^^^^^SSN~987654^Smith^John^^^^^^^^^^SV`,
    string `OBR|1|20240718-1^FOO||4520^P-INR^LAB-KL-98|||202407201257||||L||info to sample taking||||||||||||||^^^^^A|||PORT`,
    string `NTE|1||Additional info to laboratory|`
];

public function main() returns error? {
    string EXAMPLE_STR = string:'join("\r", ...EXAMPLE_ARR);

    hl7v2:Message parsedMsg = check hl7v2:parse(EXAMPLE_STR);

    hl7v23:ORM_O01 inOrm = check parsedMsg.ensureType(hl7v23:ORM_O01);

    // https://central.ballerina.io/ballerinax/health.hl7v23/3.0.3#MSH
    io:println("message type: " + inOrm.msh.msh9.cm_msg1 + "^" + inOrm.msh.msh9.cm_msg2);

    // https://central.ballerina.io/ballerinax/health.hl7v23/3.0.3#ORM_O01
    io:println("PID.5 Patient Name:");
    // patient ORM_O01_PATIENT[]
    // pid PID
    // pid5 XPN[] - patient name
    // xpn1 ST - family name
    io:println("  Family Name: " + inOrm.patient[0].pid.pid5[0].xpn1);
    io:println("  Given Name: "  + inOrm.patient[0].pid.pid5[0].xpn2);
    io:println("  Middle Name: " + inOrm.patient[0].pid.pid5[0].xpn3);
}

import ballerina/io;
public type Result record {|
    int value;
|};

public class IntSet {
    // iterable ---------------------------------------------------------------
    *object:Iterable;
    int? currentIndex;
    int maxIndex;
    int[] values_;

    public function next() returns Result? {
        if self.currentIndex is () { // object field is not narrowed by the type test :(
            self.values_ = self.values();
            self.maxIndex = self.values_.length() - 1;
            self.currentIndex = 0;
            return { value: self.values_[0]};
        } else if <int>self.currentIndex < self.maxIndex {
            self.currentIndex = self.currentIndex + 1;
            // ERROR compound assignment not allowed with nullable operands
            // self.currentIndex += 1;
            return { value: self.values_[<int>self.currentIndex] };
        } else {
            self.currentIndex = ();
            return ();
        }
    }
    
    public function iterator() returns IntSet {
        return self;
    }

    // set --------------------------------------------------------------------
    map<boolean> set;

    public function init() {
        self.currentIndex = ();
        self.maxIndex = 0;
        self.values_ = [];
        self.set = {};
    }

    public function addInt(int value) {
        self.set[self.toKey(value)] = true;
        self.currentIndex = (); // resets iteration
    }

    public function addArray(int[] arr) {
        foreach var value in arr {
            self.set[self.toKey(value)] = true;
        }
        self.currentIndex = (); // resets iteration
    }

    public function has(int value) returns boolean {
        boolean? hasValue = self.set[self.toKey(value)];
        return hasValue is () ? false : hasValue;
    }

    public function values() returns int[] {
        int[] accu = [];
        foreach var key in self.set.keys().sort() {
            accu.push(self.fromKey(key));
        }
        return accu;
    }

    public function remove(int value) {
        // check for existence first because removing non-existing value will panic
        if self.has(value) {
            var _ = self.set.remove(self.toKey(value));
            self.currentIndex = (); // resets iteration
        }
    }

    private function toKey(int value) returns string {
        return value.toString();
    }

    private function fromKey(string value) returns int {
        return checkpanic 'int:fromString(value);
    }
}

public function main() {
    IntSet set = new;
    set.addArray([1, 2, 4, 3, 2, 2, 1]);

    // foreach
    foreach var value in set {
        io:print(string`${value} `);
    }
    io:println();

    set.remove(2);
    set.addArray([5, 6]);

    // query expression
    final int[] arr = 
        from var value in set
        select value
    ;
    io:println(arr);

    foreach var value in set {
        io:print(string`${value} `);
    }
    io:println();

}
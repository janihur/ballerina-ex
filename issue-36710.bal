// https://github.com/ballerina-platform/ballerina-lang/issues/36710
import ballerina/io;
public function main() {
    string input = "1x2";

    foreach string item in input {
        match item {
            "1" => { io:println("ONE"); }
            "2" => { io:println("TWO"); }
            _ => { io:println("SOMETHING ELSE"); }
        }
    }
}
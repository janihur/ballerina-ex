// different ways to:
// 1) create error
// 2) convert error to string
import ballerina/io;
public function main() {
    io:println("err1 ---");
    error err1 = error("error 1 without details");
    io:println(err1);
    io:println(err1.toBalString());
    io:println(err1.toString());
    io:println(toStr(err1));

    io:println("err2 ---");
    error err2 = error("error 2 with details", foo = 1, bar = "42");
    io:println(err2);
    io:println(err2.toBalString());
    io:println(err2.toString());
    io:println(toStr(err2));

    io:println("err3 ---");
    error err3 = error("error 3 with cause and details", err2, car = 2, zar = "43");
    io:println(err3);
    io:println(err3.toBalString());
    io:println(err3.toString());
    io:println(toStr(err3));

    io:println("err4 ---");
    error<Error4Details> err4 = error("error 4 with details record type", intcode = 42, strcode = "IMPLEMENTATION_ERROR", details = "Whoops! We have a bug ...");
    io:println(err4);
    io:println(err4.toBalString());
    io:println(err4.toString());
    io:println(toStr(err4));
}

type Error4Details record {
    int intcode;
    string strcode;
    string details;
};

function toStr(error err) returns string {
    string s = string`(error ((message "${err.message()}")`;

    // details (optional)
    {
        var details = err.detail();
        var keys = details.keys();
        if keys.length() > 0 {
            // imperative implementation (iterates keys)
            // s += "(details (";
            // foreach string key in details.keys() {
            //     s += string`(${key} `;
            //     var value = checkpanic details.get(key);
            //     if value is string {
            //         s += string`"${value}"`;
            //     } else if value is () {
            //         s += "nil";    
            //     } else {
            //         s += value.toString();
            //     }
            //     s += ")";
            // }
            // s += "))";

            // functional implementation (iterates keys)
            s += keys.reduce(function(string allDetails, string key) returns string {
                string detail = string`(${key} `;
                any|error value = details.get(key); // value might be an error in general case
                if value is error {
                    detail += "error";
                } else if value is () {
                    detail += "nil";    
                } else if value is string {
                    detail += string`"${value}"`;
                } else {
                    detail += value.toString();
                }
                detail += ")";
                return allDetails + detail;
            }, "(details (") + "))";
        }
    }

    // cause (optional)

    // simple and straighforward implementation
    {
        error? cause = err.cause();
        if cause is error {
            s += string`(cause ${toStr(cause)})`;
        }
    }

    // clever implementation
    // s += let var e = err.cause() in e is error ? string`(cause ${toStr(e)})` : "";

    s += "))";
    return s;
}
// https://github.com/ballerina-platform/ballerina-lang/issues/34024
import ballerina/io;
public function main() {
    // xml`<a>${a}<a>` works as expected
    // but
    // xml`<ns:a>${a}</ns:a>` gives a compilation error

    xmlns "namespace" as ns;
    final xml x = xml`<ns:root xmlns:ns="namespace">
        ${from string a in ["A1", "A2"] select xml`<a>${a}</a>`}
    </ns:root>`;

    io:println("----------");
    io:println(x);
}

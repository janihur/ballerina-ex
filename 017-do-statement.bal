import ballerina/io;
public function main() {
    do {
        string s1 = "42";
        io:println(string`plain do: (s1 "${s1}")`);
    }
    do {
        string s1 = check f(false);
        io:println(string`do on fail: (s1 "${s1}")`);
        string s2 = check f(true);
        io:println(string`do on fail: (s2 "${s2}")`);
        fail error("main() oops!");
    } on fail error err{
        io:println(err);
    }
}

function f(boolean throw) returns string|error {
    var x = check g(throw);
    return x;
}

function g(boolean throw) returns string|error {
    if throw {
        return error("g() oops!");
    } else {
        return "42";
    }
}
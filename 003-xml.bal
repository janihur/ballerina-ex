import ballerina/io;
public function main() {
    do {
        io:println("#1 ----------");

        final string a = "This is A";
        final string? b = (); // "This is B";

        final xml x = xml`<root>
            <a>${a}</a>
            ${b is string ? xml`<b>${b}</b>` : xml`<b/>`}
        </root>`;

        io:println(x);
    }
    do {
        io:println("#2 ----------");

        final string a = "This is A";
        final string[] bList = []; // ["This is B"];

        final xml x = xml`<root>
            <a>${a}</a>
            ${from string b in bList select xml`<b>${b}</b>`}
        </root>`;

        io:println(x);
    }
    do {
        io:println("#3 ----------");

        xml x = xml`<foo id="1"><bar id="2">bar1</bar><bar id="3">bar2</bar></foo>`;

        xml:Element rootElem = <xml:Element> x;
        io:println(string`root element name: ${rootElem.getName()}`);
        io:println(string`root element attributes: ${rootElem.getAttributes().toString()}`);

        xml:Element someOtherElem = <xml:Element> rootElem.children()[0];
        io:println(string`some other element name: ${someOtherElem.getName()}`);
        io:println(string`some other element attributes: ${someOtherElem.getAttributes().toString()}`);

        xml barElems = x/<bar>;
        foreach xml barElem in barElems {
            if barElem is xml:Element {
                io:println(string`bar element attributes: ${barElem.getAttributes().toString()}`);
                io:println(string`bar element data: ${barElem.data()}`);
            }
        }
    }
}

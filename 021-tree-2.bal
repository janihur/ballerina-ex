import ballerina/io;

type Node record {|
    readonly int id;
    string value;
    int parentId;
    int[] childIds = [];
|};

int idSequence = 0;

function nextId() returns int {
    idSequence += 1;
    return idSequence;
}

type Tree table<Node> key(id);

public function main() {
    Tree tree = table [];

    int thisLevelParentId = 0;
    int currId = 0;
    int nextLevelParentId = 0;

    do { // root (A) ----------------------------------------------------------
        thisLevelParentId = 0;
        currId = nextId();
        nextLevelParentId = currId;

        tree.add({ id: currId, value: "A", parentId: thisLevelParentId });
    }

    do { // 1st branch (A - B - D) --------------------------------------------
        thisLevelParentId = nextLevelParentId;
        currId = nextId();
        nextLevelParentId = currId;

        tree.add({ id: currId, value: "B", parentId: thisLevelParentId });
        tree.get(thisLevelParentId).childIds.push(currId);

        thisLevelParentId = nextLevelParentId;
        currId = nextId();
        nextLevelParentId = currId;

        tree.add({ id: currId, value: "D", parentId: thisLevelParentId });
        tree.get(thisLevelParentId).childIds.push(currId);
    }
    do { // 2nd branch (A - C - E) --------------------------------------------
        thisLevelParentId = 1; // root
        currId = nextId();
        nextLevelParentId = currId;

        tree.add({ id: currId, value: "C", parentId: thisLevelParentId });
        tree.get(thisLevelParentId).childIds.push(currId);

        thisLevelParentId = nextLevelParentId;
        currId = nextId();
        nextLevelParentId = currId;

        tree.add({ id: currId, value: "E", parentId: thisLevelParentId });
        tree.get(thisLevelParentId).childIds.push(currId);
    }

    io:println(tree);

    do { // from bottom to up -------------------------------------------------
        Node currentNode = (
            from Node nextNode in tree
            where nextNode.value == "D"
            limit 1
            select nextNode
        )[0];

        io:println(currentNode.value);

        currentNode = (
            from Node nextNode in tree
            where nextNode.id == currentNode.parentId
            limit 1
            select nextNode
        )[0];

        io:println(currentNode.value);
    }
}
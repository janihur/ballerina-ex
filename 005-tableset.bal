import ballerina/io;

type Coord record {|
    readonly int x;
    readonly int y;
|};

public function main() {
    do {
        table<Coord> key(x, y) validCoordinates = table [
            {x:0, y:0},
            {x:1, y:0}
        ];

        validCoordinates.add({x:2, y:0});

        foreach var coord in validCoordinates {
            io:println(coord);
        }

        var _ = validCoordinates.remove([2, 0]);

        Coord? c1 = validCoordinates[0, 0];
        io:println(c1 ?: "[0, 0] not available");

        Coord? c2 = validCoordinates[2, 0];
        io:println(c2 ?: "[2, 0] not available");
    }

    do {
        table<Coord> key(x, y) a = table [
            {x:0, y:0},
            {x:1, y:0}
        ];
        table<Coord> key(x, y) b = table [
            {x:0, y:0},
            {x:1, y:0},
            {x:2, y:0}
        ];

        if a == b {
            io:println("a equals b");
        } else {
            io:println("a doesn't equal b");
        }
    }
}
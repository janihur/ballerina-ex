// https://github.com/ballerina-platform/ballerina-lang/issues/35753
import ballerina/io;
public function main() {
    io:println("hello");
    {
        string hello = "hello";
        io:println(hello);
    }
    {
        // undefined function println
        io:println("hello");
    }
}
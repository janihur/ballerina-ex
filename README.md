# Ballerina Example

[Ballerina Tool](https://ballerina.io/learn/update-tool/):
```
# update the tool
bal update
# update the ballerina distribution
bal dist update
```

Create new package (program):
```
bal new <PACKAGE>
```

```shell
# run the package
bal run <PACKAGE>
```

## Character Encoding

File: `022-character-encoding.bal`

Usage:
```shell
# print content
bal run 022-character-encoding.bal -- 022-iso-8859-15.txt ISO-8859-15
# write content to a file with different encoding
bal run 022-character-encoding.bal -- 022-iso-8859-15.txt ISO-8859-15 windows-1252
```

* [Western Latin Character Sets](https://en.wikipedia.org/wiki/Western_Latin_character_sets_(computing))
* [ISO-8851-15](https://en.wikipedia.org/wiki/ISO/IEC_8859-15)
* [Windows-1252](https://en.wikipedia.org/wiki/Windows-1252)
* [JDK17 Supported Encodings](https://docs.oracle.com/en/java/javase/17/intl/supported-encodings.html) (the JDK version the jBallerina implementation is based on the time of writing)


|Character|UTF-8           |ISO-8859-15|Windows-1252|
|---------|----------------|-----------|------------|
|A        |`0x41`          |`0x41`     |`0x41`      |
|B        |`0x42`          |`0x42`     |`0x42`      |
|C        |`0x43`          |`0x43`     |`0x43`      |
|a        |`0x61`          |`0x61`     |`0x61`      |
|b        |`0x62`          |`0x62`     |`0x62`      |
|c        |`0x63`          |`0x63`     |`0x63`      |
|€        |`0xE2 0x82 0xAC`|`0xA4`     |`0x80`      |
|Ä        |`0xC3 0x84`     |`0xC4`     |`0xC4`      |
|Å        |`0xC3 0x85`     |`0xC5`     |`0xC5`      |
|Ö        |`0xC3 0x96`     |`0xD6`     |`0xD6`      |
|ä        |`0xC3 0xA4`     |`0xE4`     |`0xE4`      |
|å        |`0xC3 0xA5`     |`0xE5`     |`0xE5`      |
|ö        |`0xC3 0xB6`     |`0xF6`     |`0xF6`      |


In the example below `0x0a` is new line.
```
$ file 022-utf-8.txt
022-utf-8.txt: Unicode text, UTF-8 text
$ xxd 022-utf-8.txt
00000000: 4142 430a 6162 630a e282 ac0a c384 c385  ABC.abc.........
00000010: c396 0ac3 a4c3 a5c3 b60a                 ..........

$ file 022-iso8859-15.txt
022-iso8859-15.txt: ISO-8859 text
$ xxd 022-iso8859-15.txt
00000000: 4142 430a 6162 630a a40a c4c5 d60a e4e5  ABC.abc.........
00000010: f60a                                     ..

$ file 022-windows-1252.txt
022-windows-1252.txt: Non-ISO extended-ASCII text
$ xxd 022-windows-1252.txt
00000000: 4142 430a 6162 630a 800a c4c5 d60a e4e5  ABC.abc.........
00000010: f60a                                     ..
```
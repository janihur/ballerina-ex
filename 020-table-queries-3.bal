import ballerina/io;
import ballerina/test;

// ----------------------------------------------------------------------------
// #1) one-way one-to-many relation
// one Foo can have any number of Bars
// you can only navigate from Foo to Bar
type Foo record {|
    readonly int id;
    string data;
    int[] barIds;
|};
type FooTable table<Foo> key(id);

type Bar record {|
    readonly int id;
    string data;
|};
type BarTable table<Bar> key(id);

type FooBarTable table<record {|readonly int fooId; readonly int barId; string data;|}> key(fooId, barId);

public function main() {
    final readonly & FooTable foos = table [
        {id: 1, data: "foo 1", barIds: [2, 4]},
        {id: 2, data: "foo 2", barIds: [99]}, // no such barId
        {id: 3, data: "foo 3", barIds: [4, 6]},
        {id: 4, data: "foo 2", barIds: []},
        {id: 5, data: "foo 5", barIds: [6]}
    ];

    final readonly & BarTable bars = table [
        {id: 2, data: "bar 2"},
        {id: 4, data: "bar 4"},
        {id: 6, data: "bar 6"}
    ];

    final readonly & FooBarTable expected = table [
        {fooId: 1, barId: 2, data: "foo 1;bar 2"},
        {fooId: 1, barId: 4, data: "foo 1;bar 4"},
        {fooId: 3, barId: 4, data: "foo 3;bar 4"},
        {fooId: 3, barId: 6, data: "foo 3;bar 6"},
        {fooId: 5, barId: 6, data: "foo 5;bar 6"}
    ];

    do {
        // https://codereview.stackexchange.com/a/280489/15012
        // note inner join, so drops all foos not having bar

        FooBarTable fooBars = table key(fooId, barId)
            from var foo in foos
            from var barId in foo.barIds where bars.hasKey(barId)
            let Bar bar = bars.get(barId)
            select {
                fooId: foo.id,
                barId: barId,
                data: foo.data + ";" + bar.data
            };

        io:println(fooBars);
        test:assertEquals(fooBars, expected);
    }

    do {
        // https://codereview.stackexchange.com/a/281138/15012
        // originally from Ballerina Discord comment
        // note inner join, so drops all foos not having bar

        FooBarTable fooBars = table key(fooId, barId)
            from Bar bar in bars
            from Foo foo in foos where foo.barIds.indexOf(bar.id) != ()
            select {
                fooId: foo.id, 
                barId: bar.id, 
                data: string`${foo.data};${bar.data}`
            };	

        io:println(fooBars);
        test:assertEquals(fooBars, expected);
   }

   do {
        // 3rd variation
        // note inner join, so drops all foos not having bar

        FooBarTable fooBars = table key(fooId, barId)
            from var foo in foos
            let var barList = foo.barIds
            from var barId in barList where bars.hasKey(barId)
            let Bar bar = bars.get(barId)

            select {
                fooId: foo.id,
                barId: bar.id,
                data: string`${foo.data};${bar.data}`
            };

        io:println(fooBars);
        test:assertEquals(fooBars, expected);
   }
}
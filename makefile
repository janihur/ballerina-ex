SRCS := $(wildcard ???-*.bal)
JARS := $(addprefix target/, $(addsuffix .jar, $(basename ${SRCS})))

all: ${JARS}

.PHONY: clean
clean:
	@rm -rf *~ target/

target:
	@mkdir -p $@

target/%.jar: %.bal | target
	@tput bold; echo '----------------'; tput sgr0
	@time -f 'Build time: %e seconds' bal build --offline --output $@ $<

import ballerina/io;

type Int record {
    readonly int i;
};

public function main() {
    int[] input = [1,2,6,4,5,6,7,8,5,3,4,2,2,3,4,5,6,6];

    table<Int> key(i) intSet = table key(i) 
        from var i in input
        order by i
        select {i}
    ;

    io:println(intSet);

    io:println(intSet.keys());

    // sorting example
    int[] x = from int i in input
        order by i
        select i
    ;

    io:println(x);   
}

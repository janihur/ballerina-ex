import ballerina/io;

public type Result record {|
    int value;
|};

# input: currect value
# returns: next value
type StepFn function(int) returns int;

# input: current value, final value
# returns: true when last iteration
type StopFn function(int, int) returns boolean;

public class GenericIntIter {
    int c;
    final int end;
    final StepFn stepFn;
    final StopFn stopFn;

    function init(int beg, int end, StepFn stepFn, StopFn stopFn) {
        self.c = beg;
        self.end = end;
        self.stepFn = stepFn;
        self.stopFn = stopFn;
    }

    // Implements the `next` method
    public function next() returns Result? {
        if self.stopFn(self.c, self.end) {
            return ();
        }
        Result r = {value: self.c};
        self.c = self.stepFn(self.c);
        return r;
    }
}

public class GenericIntIterable {
    // Need to have this type inclusion as to indicate the distinct type relationship
    *object:Iterable;

    final int beg;
    final int end;
    final StepFn stepFn;
    final StopFn stopFn;

    function init(int beg, int end, StepFn stepFn, StopFn stopFn) {
        self.beg = beg;
        self.end = end;
        self.stepFn = stepFn;
        self.stopFn = stopFn;
    }

    public function iterator() returns GenericIntIter {
        return new GenericIntIter(self.beg, self.end, self.stepFn, self.stopFn);
    }
}

# Inclusive decrementing iterator (lazy evaluation)
public class InclusiveDecIter {
    int c;
    final int end;

    function init(int beg, int end) {
        self.c = beg;
        self.end = end;
    }

    // Implements the `next` method
    public function next() returns Result? {
        if self.c < self.end {
            return ();
        }
        Result r = {value: self.c};
        self.c -= 1;
        return r;
    }
}

public class IntDecIterable {
    // Need to have this type inclusion as to indicate the distinct type relationship
    *object:Iterable;

    final int beg;
    final int end;

    function init(int beg, int end) {
        self.beg = beg;
        self.end = end;
    }

    public function iterator() returns InclusiveDecIter {
        return new InclusiveDecIter(self.beg, self.end);
    }
}

public class InclusiveIncIter {
    int c;
    final int max;

    function init(int min, int max) {
        self.c = min;
        self.max = max;
    }

    // Implements the `next` method
    public function next() returns Result? {
        if self.c > self.max {
            return ();
        }
        Result r = {value: self.c};
        self.c += 1;
        return r;
    }
}

public class IntIncIterable {
    // Need to have this type inclusion as to indicate the distinct type relationship
    *object:Iterable;

    final int min;
    final int max;

    function init(int min, int max) {
        self.min = min;
        self.max = max;
    }
    
    public function iterator() returns InclusiveIncIter {
        return new InclusiveIncIter(self.min, self.max);
    }
}

public function main() {
    IntIncIterable iter = new IntIncIterable(3, 9);
    foreach var i in iter {
        io:print(i);
    }
    io:println();

    foreach var i in new IntDecIterable(9, 3) {
        io:print(i);
    }
    io:println();

    final int[] dec = 
        from var i in 
        new GenericIntIterable(
            beg = 99, end = 33, 
            stepFn = cur => cur - 1, 
            stopFn = (cur, end) => cur < end
        ) 
        where i % 3 == 0 
        select i
    ;
    io:println(dec);

    final int[] inc = 
        from var i in 
        new GenericIntIterable(
            beg = 33, end = 99, 
            stepFn = cur => cur + 1, 
            stopFn = (cur, end) => cur > end
        ) 
        where i % 3 == 0 
        select i
    ;
    io:println(inc);
}
// https://github.com/ballerina-platform/ballerina-spec/issues/602
import ballerina/io;

public function main() {
    io:println(sum1([1,2,3,4], [0,1]));
    io:println(sum2([1,2,3,4], [0,1]));
}

isolated function sum1(int[] numbers, int[] indexes) returns int {
    return indexes.reduce(isolated function (int accu, int index) returns int {
        // ERROR invalid access of mutable storage in an 'isolated' function
        return accu + numbers[index];
    }, 0);
}

isolated function sum2(int[] numbers, int[] indexes) returns int {
    int accu = 0;
    foreach int index in indexes {
        // but here we can access
        accu += numbers[index];
    }
    return accu;
}
import ballerina/io;
import ballerina/time;

public function main() {
    xmlns "http://tempuri.org/" as t;
    xmlns "http://schemas.datacontract.org/2004/07/DigitalkkaritWCF" as a;

    // xml navigation that is not xpath

    xml getFreeTimesResponse = inputXml/**/<t:GetFreeTimesResponse>;
    // io:println(getFreeTimesResponse);

    xml timeSlots = getFreeTimesResponse/**/<a:TimeSlot>;
    // io:println(timeSlots);

    // foreach var timeSlot in timeSlots {
    //     io:println(timeSlot);
    // }

    // map<map<string>>[] validForList = 
    json[] validForList =
        from xml timeSlot in timeSlots
        where timeSlot is xml:Element
        let xml startElem = timeSlot/<a:Start>
        let xml endElem = timeSlot/<a:End>
        select {
            validFor: {
                startDateTime: startElem.data(),
                endDateTime: endElem.data(),
                "@type": "TimeSlotDigitalkkarit",
                "@baseType": "TimeSlot"
            }
        }
    ;

    // io:println(validForList);

    json result = {
        searchDate: time:utcToString(time:utcNow(3)),
        searchResult: "success",
        availableTimeSlot: validForList
    };

    io:println(result);
}

// output JSON
// {
// 	"searchDate": "2018-02-01T14:40:43.000Z",
// 	"searchResult": "success",
// 	"availableTimeSlot": [
// 		{
// 			"validFor": {
// 				"startDateTime": "2018-02-15T14:00:00.000Z",
// 				"endDateTime": "2018-02-15T16:00:00.000Z",
// 				"@type": "TimeSlotDigitalkkarit",
// 				"@baseType": "TimeSlot"
// 			}
// 		},
// 		{
// 			"validFor": {
// 				"startDateTime": "2018-02-16T10:00:00.000Z",
// 				"endDateTime": "2018-02-16T12:00:00.000Z",
// 				"@type": "TimeSlotDigitalkkarit",
// 				"@baseType": "TimeSlot"
// 			}
// 		}
// 	]
// }

// input XML
final readonly & xml inputXml = xml`<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header><ActivityId xmlns="http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics" CorrelationId="b7d8b456-60cc-495f-88e4-5c2f095ffbd9">00000000-0000-0000-0000-000000000000</ActivityId></s:Header><s:Body><GetFreeTimesResponse xmlns="http://tempuri.org/"><GetFreeTimesResult xmlns:a="http://schemas.datacontract.org/2004/07/DigitalkkaritWCF" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:Code>OK</a:Code><a:Details>OK</a:Details><a:TimeSlots><a:TimeSlot><a:End>2022-03-28T09:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-28T09:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-28T10:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-28T10:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-28T11:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-28T11:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-28T12:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-28T12:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-28T13:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-28T13:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-28T14:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-28T14:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-28T15:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-28T15:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-29T09:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-29T09:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-29T10:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-29T10:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-29T11:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-29T11:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-29T12:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-29T12:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-29T13:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-29T13:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-29T14:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-29T14:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-29T15:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-29T15:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-30T10:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-30T10:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-30T11:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-30T11:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-30T12:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-30T12:00:00+03:00</a:Start></a:TimeSlot><a:TimeSlot><a:End>2022-03-30T13:45:00+03:00</a:End><a:Installer i:nil="true"/><a:Start>2022-03-30T13:00:00+03:00</a:Start></a:TimeSlot></a:TimeSlots></GetFreeTimesResult></GetFreeTimesResponse></s:Body></s:Envelope>`;
// how to construct (open) records and how to iterate the members
import ballerina/io;
public function main() {
    f2();
}

function f1() {
    FooBar fb = {foo:42, bar: "meaning of life"};
    var entries = fb.entries();
    io:println(entries);
    var keys = fb.keys();
    io:println(keys);
    var arr = fb.toArray();
    io:println(arr);

    foreach var item in entries {
        var [key, value] = item;
        io:println(kv(key, value));
    }
}

function f2() {
    record {int a; string b; string? c; float d;} x = {
        a: 1,
        b: "foo's bar",
        c: (),
        d: 3.14
    };

    foreach [string, any] item in x.entries() {
        [string, any] [key, value] = item;
        io:print(kv(key, value));
    }
    io:println();

    final string s = x.entries().reduce(function(string accu, [string, any] item) returns string {
        return accu + kv(item[0], item[1]);
    }, "");
    io:println(s);
}

function kv(string k, any v) returns string {
    string value;
    if v is string {
        value = string`"${v}"`;
    } else if v is () {
        value = "nil";
    } else {
        value =  v.toString();
    }
    return string`(${k} ${value})`;
}

type FooBar record {|
    int foo;
    string bar;
|};
type FooBar2 record {|
    int foo;
    string bar;
|};
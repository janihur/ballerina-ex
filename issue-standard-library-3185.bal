// https://github.com/ballerina-platform/ballerina-standard-library/issues/3185
import ballerina/http;
import ballerina/io;

service class RequestInterceptor1 {
    *http:RequestInterceptor;

    resource function 'default [string... path](
        http:RequestContext ctx, 
        http:Request req
    ) returns http:NextService|error? {
        req.setHeader("X-Correlation-Id", "1234");
        // expected value is printed:
        io:println("userAgent: ", req.userAgent);
        return ctx.next();
    }
}
RequestInterceptor1 requestInterceptor1 = new;

@http:ServiceConfig {
    interceptors: [ requestInterceptor1 ]
}
service / on new http:Listener(9090) {
    resource function get . (http:Request r) returns string{
        io:println("---");
        string[] headers = r.getHeaderNames();
        foreach var header in headers {
            io:println(header);
        }
        io:println("---");
        // unexpectedly nothing is printed:
        io:println("userAgent: ", r.userAgent);
        return "hello";
    }
}
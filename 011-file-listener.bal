// https://ballerina.io/learn/by-example/directory-listener.html
// https://lib.ballerina.io/ballerina/file/latest

//
// assumes in/outbox directories exists
//
// processes single line xml book files:
//
// tmp$ cat book1 book2
// <book>Animal Farm</book>
// <book>Hamlet</book>
//
// tmp$ cp book1 book2 bal-inbox/
//
// tmp$ tree bal-inbox/ bal-outbox/
// bal-inbox/
// bal-outbox/
// ├── book1
// └── book2
// 0 directories, 2 files
// 
// tmp$ cat bal-outbox/book?
// <book>*** CENSORED ***</book><book>Hamlet</book>
//

import ballerina/file;
import ballerina/io;
import ballerina/log;

const string BAL_INBOX = "/tmp/bal-inbox";
const string BAL_OUTBOX = "/tmp/bal-outbox";

listener file:Listener inbox = new ({
    path: BAL_INBOX,
    recursive: false
});

service "localObserver" on inbox {
    remote function onCreate(file:FileEvent fe) {
        log:printInfo(fe.toBalString());

        file:MetaData|file:Error md = file:getMetaData(fe.name);
        if md is file:MetaData {
            log:printInfo(md.toBalString());
            process(md);
        } else {
            log:printError("Failed to read file metadata.", md);
        }
    }

    remote function onDelete(file:FileEvent fe) {
        log:printInfo(fe.toBalString());

        // metadata not available for deleted file
        file:MetaData|file:Error md = file:getMetaData(fe.name);
        if md is file:MetaData {
            log:printInfo(md.toBalString());
        } else {
            log:printError("Failed to read file metadata.", md);
        }
    }
 
    remote function onModify(file:FileEvent fe) {
        log:printInfo(fe.toBalString());

        file:MetaData|file:Error md = file:getMetaData(fe.name);
        if md is file:MetaData {
            log:printInfo(md.toBalString());
        } else {
            log:printError("Failed to read file metadata.", md);
        }
   }
}

// the error handling of each step is done in place for demonstration - it could be improved.
function process(file:MetaData md) {
    final string originalPath = md.absPath;
    final string newPath = BAL_OUTBOX + "/" + basename(md.absPath);
    
    log:printInfo(string`BEGIN processing file.`, originalPath = originalPath, newPath = newPath);

    xml|io:Error originalContent = io:fileReadXml(originalPath);
    if originalContent is xml { // process and move from inbox to outbox
        string title = originalContent.<book>.data();
        if title.toLowerAscii() == "animal farm" { // processing required
            xml newContent = xml`<book>*** CENSORED ***</book>`;
            io:Error? writeErr = io:fileWriteXml(newPath, newContent);
            if writeErr is () {
                file:Error? removeErr = file:remove(originalPath);
                if removeErr is file:Error {
                    log:printError("Failed to remove original file.", removeErr, originalPath = originalPath);
                }
            } else {
                log:printError("Failed to create new processed file.", writeErr, newPath = newPath);
            }
        } else { // content ok, move to outbox
            file:Error? renameErr = file:rename(originalPath, newPath);
            if renameErr is file:Error {
                log:printError("Failed to move file from inbox to outbox", renameErr, originalPath = originalPath, newPath = newPath);
            }
        }
    } else {
        log:printError("Failed to read file as xml file.", originalContent, originalPath = originalPath);
    }

    log:printInfo(string`END processing file.`, originalPath = originalPath, newPath = newPath);
}

// all paths are in our control so basename should "never" fail
// so if it fails we could crash (we're missing top level trap thought)
function basename(string path) returns string {
    return checkpanic file:basename(path);
}
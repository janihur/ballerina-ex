import ballerina/io;
import ballerina/time;
import ballerina/lang.'decimal as decimal;

public function main() {
    io:println("utc -----");
    io:println(string`time:utcToString = ${time:utcToString(time:utcNow(3))}`);

    io:println("local -----");
    time:TimeOfDay now = localNow();
    io:println(string`time:TimeOfDay = ${now.toString()}`);
    
    string nowStr = timeOfDayToString(now);
    io:println(string`string = ${nowStr}`);
}

function padLeft(int? number, int? size = ()) returns string {
    if number is int {
        string iStr = number.toString();
        if size is int {
            foreach var _ in iStr.length() ..< size {
                iStr = "0" + iStr;
            }
        }
        return iStr;
    }
    return "";
}

function timeOfDayToString(time:TimeOfDay t) returns string {
    string year   = padLeft(t.year, size = 4);
    string month  = padLeft(t.month, size = 2);
    string day    = padLeft(t.day, size = 2);
    string hour   = padLeft(t.hour, size = 2);
    string minute = padLeft(t.minute, size = 2);
    string second = padLeft(<int>t.second, size = 2);

    return string`${year}-${month}-${day}T${hour}:${minute}:${second}`;
}

function localNow() returns time:TimeOfDay {
    time:Zone tz = checkpanic time:loadSystemZone();
    time:Civil civil = tz.utcToCivil(time:utcNow());

    return <time:TimeOfDay> {
        year: civil.year,
        month: civil.month,
        day: civil.day,
        hour: civil.hour,
        minute: civil.minute,
        second: decimal:floor(civil.second ?: <time:Seconds>0)
    };
}
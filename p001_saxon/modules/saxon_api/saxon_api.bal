import p001_saxon.saxon_bindings as saxon;
public class Xslt {
    saxon:Processor processor;
    saxon:XsltExecutable executable;

    public function init(StreamSource stylesheet) returns error? {
        self.processor = saxon:newProcessor1(false);
        saxon:XsltCompiler compiler = self.processor.newXsltCompiler();
        self.executable = check compiler.compile2(stylesheet.streamSource);
    }
    
    public function transform(StreamSource input) returns string|error {
        saxon:StringWriter stringWriter = saxon:newStringWriter1();
        saxon:Serializer serializer = self.processor.newSerializer4(stringWriter);

        saxon:Xslt30Transformer transformer = self.executable.load30();

        check transformer.transform2(input.streamSource, serializer);

        return stringWriter.jObj.toString();
    }
}

public class StreamSource {
    saxon:StreamSource streamSource;
    public function init(string fileName) returns error? {
        saxon:FileInputStream fis = check saxon:newFileInputStream3(fileName);
        self.streamSource = saxon:newStreamSource3(fis);
    }
}
// public function readFile(string fileName) returns saxon:StreamSource|error {
//     saxon:FileInputStream fis = check saxon:newFileInputStream3(fileName);

//     saxon:StreamSource ss = saxon:newStreamSource3(fis);
//     // saxon:Source s = check java:cast(ss);

//     return ss;
// }
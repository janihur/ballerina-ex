import ballerina/jballerina.java;
import ballerina/jballerina.java.arrays as jarrays;

# Ballerina class mapping for the Java `net.sf.saxon.s9api.Xslt30Transformer` class.
@java:Binding {'class: "net.sf.saxon.s9api.Xslt30Transformer"}
public distinct class Xslt30Transformer {

    *java:JObject;
    *Object;

    # The `handle` field that stores the reference to the `net.sf.saxon.s9api.Xslt30Transformer` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `net.sf.saxon.s9api.Xslt30Transformer` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `net.sf.saxon.s9api.Xslt30Transformer` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `applyTemplates` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Source` value required to map with the Java method parameter.
    # + return - The `XdmValue` or the `SaxonApiException` value returning from the Java mapping.
    public function applyTemplates(Source arg0) returns XdmValue|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_Xslt30Transformer_applyTemplates(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XdmValue newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `applyTemplates` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Source` value required to map with the Java method parameter.
    # + arg1 - The `Destination` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function applyTemplates2(Source arg0, Destination arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_applyTemplates2(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `applyTemplates` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `XdmValue` value required to map with the Java method parameter.
    # + return - The `XdmValue` or the `SaxonApiException` value returning from the Java mapping.
    public function applyTemplates3(XdmValue arg0) returns XdmValue|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_Xslt30Transformer_applyTemplates3(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XdmValue newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `applyTemplates` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `XdmValue` value required to map with the Java method parameter.
    # + arg1 - The `Destination` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function applyTemplates4(XdmValue arg0, Destination arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_applyTemplates4(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `asDocumentDestination` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Destination` value required to map with the Java method parameter.
    # + return - The `Destination` value returning from the Java mapping.
    public function asDocumentDestination(Destination arg0) returns Destination {
        handle externalObj = net_sf_saxon_s9api_Xslt30Transformer_asDocumentDestination(self.jObj, arg0.jObj);
        Destination newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `callFunction` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `QName` value required to map with the Java method parameter.
    # + arg1 - The `XdmValue[]` value required to map with the Java method parameter.
    # + return - The `XdmValue` or the `SaxonApiException` value returning from the Java mapping.
    public function callFunction(QName arg0, XdmValue[] arg1) returns XdmValue|SaxonApiException|error {
        handle|error externalObj = net_sf_saxon_s9api_Xslt30Transformer_callFunction(self.jObj, arg0.jObj, check jarrays:toHandle(arg1, "net.sf.saxon.s9api.XdmValue"));
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XdmValue newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `callFunction` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `QName` value required to map with the Java method parameter.
    # + arg1 - The `XdmValue[]` value required to map with the Java method parameter.
    # + arg2 - The `Destination` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function callFunction2(QName arg0, XdmValue[] arg1, Destination arg2) returns SaxonApiException?|error? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_callFunction2(self.jObj, arg0.jObj, check jarrays:toHandle(arg1, "net.sf.saxon.s9api.XdmValue"), arg2.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `callTemplate` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `QName` value required to map with the Java method parameter.
    # + return - The `XdmValue` or the `SaxonApiException` value returning from the Java mapping.
    public function callTemplate(QName arg0) returns XdmValue|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_Xslt30Transformer_callTemplate(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XdmValue newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `callTemplate` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `QName` value required to map with the Java method parameter.
    # + arg1 - The `Destination` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function callTemplate2(QName arg0, Destination arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_callTemplate2(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `equals` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return net_sf_saxon_s9api_Xslt30Transformer_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = net_sf_saxon_s9api_Xslt30Transformer_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `hashCode` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return net_sf_saxon_s9api_Xslt30Transformer_hashCode(self.jObj);
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer() returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Xslt30Transformer_newSerializer(self.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `File` value required to map with the Java method parameter.
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer2(File arg0) returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Xslt30Transformer_newSerializer2(self.jObj, arg0.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `OutputStream` value required to map with the Java method parameter.
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer3(OutputStream arg0) returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Xslt30Transformer_newSerializer3(self.jObj, arg0.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Writer` value required to map with the Java method parameter.
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer4(Writer arg0) returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Xslt30Transformer_newSerializer4(self.jObj, arg0.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `notify` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    public function notify() {
        net_sf_saxon_s9api_Xslt30Transformer_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    public function notifyAll() {
        net_sf_saxon_s9api_Xslt30Transformer_notifyAll(self.jObj);
    }

    # The function that maps to the `setGlobalContextItem` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `XdmItem` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function setGlobalContextItem(XdmItem arg0) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_setGlobalContextItem(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `setGlobalContextItem` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `XdmItem` value required to map with the Java method parameter.
    # + arg1 - The `boolean` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function setGlobalContextItem2(XdmItem arg0, boolean arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_setGlobalContextItem2(self.jObj, arg0.jObj, arg1);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `setInitialContextItem` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `XdmItem` value required to map with the Java method parameter.
    public function setInitialContextItem(XdmItem arg0) {
        net_sf_saxon_s9api_Xslt30Transformer_setInitialContextItem(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setInitialContextNode` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `XdmNode` value required to map with the Java method parameter.
    public function setInitialContextNode(XdmNode arg0) {
        net_sf_saxon_s9api_Xslt30Transformer_setInitialContextNode(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setInitialTemplateParameters` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Map` value required to map with the Java method parameter.
    # + arg1 - The `boolean` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function setInitialTemplateParameters(Map arg0, boolean arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_setInitialTemplateParameters(self.jObj, arg0.jObj, arg1);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `setStylesheetParameters` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Map` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function setStylesheetParameters(Map arg0) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_setStylesheetParameters(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `transform` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `Source` value required to map with the Java method parameter.
    # + arg1 - The `Destination` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function transform(Source arg0, Destination arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_transform(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    public function transform2(StreamSource arg0, Serializer arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_transform2(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Xslt30Transformer`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Xslt30Transformer_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

function net_sf_saxon_s9api_Xslt30Transformer_applyTemplates(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "applyTemplates",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["javax.xml.transform.Source"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_applyTemplates2(handle receiver, handle arg0, handle arg1) returns error? = @java:Method {
    name: "applyTemplates",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["javax.xml.transform.Source", "net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_applyTemplates3(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "applyTemplates",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.XdmValue"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_applyTemplates4(handle receiver, handle arg0, handle arg1) returns error? = @java:Method {
    name: "applyTemplates",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.XdmValue", "net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_asDocumentDestination(handle receiver, handle arg0) returns handle = @java:Method {
    name: "asDocumentDestination",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_callFunction(handle receiver, handle arg0, handle arg1) returns handle|error = @java:Method {
    name: "callFunction",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.QName", "[Lnet.sf.saxon.s9api.XdmValue;"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_callFunction2(handle receiver, handle arg0, handle arg1, handle arg2) returns error? = @java:Method {
    name: "callFunction",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.QName", "[Lnet.sf.saxon.s9api.XdmValue;", "net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_callTemplate(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "callTemplate",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.QName"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_callTemplate2(handle receiver, handle arg0, handle arg1) returns error? = @java:Method {
    name: "callTemplate",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.QName", "net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["java.lang.Object"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Xslt30Transformer_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Xslt30Transformer_newSerializer(handle receiver) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Xslt30Transformer_newSerializer2(handle receiver, handle arg0) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["java.io.File"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_newSerializer3(handle receiver, handle arg0) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["java.io.OutputStream"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_newSerializer4(handle receiver, handle arg0) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["java.io.Writer"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Xslt30Transformer_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Xslt30Transformer_setGlobalContextItem(handle receiver, handle arg0) returns error? = @java:Method {
    name: "setGlobalContextItem",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.XdmItem"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_setGlobalContextItem2(handle receiver, handle arg0, boolean arg1) returns error? = @java:Method {
    name: "setGlobalContextItem",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.XdmItem", "boolean"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_setInitialContextItem(handle receiver, handle arg0) = @java:Method {
    name: "setInitialContextItem",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.XdmItem"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_setInitialContextNode(handle receiver, handle arg0) = @java:Method {
    name: "setInitialContextNode",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["net.sf.saxon.s9api.XdmNode"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_setInitialTemplateParameters(handle receiver, handle arg0, boolean arg1) returns error? = @java:Method {
    name: "setInitialTemplateParameters",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["java.util.Map", "boolean"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_setStylesheetParameters(handle receiver, handle arg0) returns error? = @java:Method {
    name: "setStylesheetParameters",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["java.util.Map"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_transform(handle receiver, handle arg0, handle arg1) returns error? = @java:Method {
    name: "transform",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["javax.xml.transform.Source", "net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_transform2(handle receiver, handle arg0, handle arg1) returns error? = @java:Method {
    name: "transform",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["javax.xml.transform.stream.StreamSource", "net.sf.saxon.s9api.Serializer"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Xslt30Transformer_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["long"]
} external;

function net_sf_saxon_s9api_Xslt30Transformer_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Xslt30Transformer",
    paramTypes: ["long", "int"]
} external;


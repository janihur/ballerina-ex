import ballerina/jballerina.java;

# Ballerina class mapping for the Java `net.sf.saxon.s9api.XsltExecutable` class.
@java:Binding {'class: "net.sf.saxon.s9api.XsltExecutable"}
public distinct class XsltExecutable {

    *java:JObject;
    *Object;

    # The `handle` field that stores the reference to the `net.sf.saxon.s9api.XsltExecutable` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `net.sf.saxon.s9api.XsltExecutable` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `net.sf.saxon.s9api.XsltExecutable` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `equals` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return net_sf_saxon_s9api_XsltExecutable_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `explain` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + arg0 - The `Destination` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function explain(Destination arg0) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_XsltExecutable_explain(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `export` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + arg0 - The `OutputStream` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function export(OutputStream arg0) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_XsltExecutable_export(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `export` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + arg0 - The `OutputStream` value required to map with the Java method parameter.
    # + arg1 - The `string` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function export2(OutputStream arg0, string arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_XsltExecutable_export2(self.jObj, arg0.jObj, java:fromString(arg1));
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `getClass` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = net_sf_saxon_s9api_XsltExecutable_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getGlobalParameters` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `HashMap` value returning from the Java mapping.
    public function getGlobalParameters() returns HashMap {
        handle externalObj = net_sf_saxon_s9api_XsltExecutable_getGlobalParameters(self.jObj);
        HashMap newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getProcessor` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `Processor` value returning from the Java mapping.
    public function getProcessor() returns Processor {
        handle externalObj = net_sf_saxon_s9api_XsltExecutable_getProcessor(self.jObj);
        Processor newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getUnderlyingCompiledStylesheet` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `PreparedStylesheet` value returning from the Java mapping.
    public function getUnderlyingCompiledStylesheet() returns PreparedStylesheet {
        handle externalObj = net_sf_saxon_s9api_XsltExecutable_getUnderlyingCompiledStylesheet(self.jObj);
        PreparedStylesheet newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getWhitespaceStrippingPolicy` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `WhitespaceStrippingPolicy` value returning from the Java mapping.
    public function getWhitespaceStrippingPolicy() returns WhitespaceStrippingPolicy {
        handle externalObj = net_sf_saxon_s9api_XsltExecutable_getWhitespaceStrippingPolicy(self.jObj);
        WhitespaceStrippingPolicy newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `hashCode` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return net_sf_saxon_s9api_XsltExecutable_hashCode(self.jObj);
    }

    # The function that maps to the `load` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `XsltTransformer` value returning from the Java mapping.
    public function load() returns XsltTransformer {
        handle externalObj = net_sf_saxon_s9api_XsltExecutable_load(self.jObj);
        XsltTransformer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `load30` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `Xslt30Transformer` value returning from the Java mapping.
    public function load30() returns Xslt30Transformer {
        handle externalObj = net_sf_saxon_s9api_XsltExecutable_load30(self.jObj);
        Xslt30Transformer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `notify` method of `net.sf.saxon.s9api.XsltExecutable`.
    public function notify() {
        net_sf_saxon_s9api_XsltExecutable_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `net.sf.saxon.s9api.XsltExecutable`.
    public function notifyAll() {
        net_sf_saxon_s9api_XsltExecutable_notifyAll(self.jObj);
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_XsltExecutable_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_XsltExecutable_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.XsltExecutable`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_XsltExecutable_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

function net_sf_saxon_s9api_XsltExecutable_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: ["java.lang.Object"]
} external;

function net_sf_saxon_s9api_XsltExecutable_explain(handle receiver, handle arg0) returns error? = @java:Method {
    name: "explain",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: ["net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_XsltExecutable_export(handle receiver, handle arg0) returns error? = @java:Method {
    name: "export",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: ["java.io.OutputStream"]
} external;

function net_sf_saxon_s9api_XsltExecutable_export2(handle receiver, handle arg0, handle arg1) returns error? = @java:Method {
    name: "export",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: ["java.io.OutputStream", "java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltExecutable_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_getGlobalParameters(handle receiver) returns handle = @java:Method {
    name: "getGlobalParameters",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_getProcessor(handle receiver) returns handle = @java:Method {
    name: "getProcessor",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_getUnderlyingCompiledStylesheet(handle receiver) returns handle = @java:Method {
    name: "getUnderlyingCompiledStylesheet",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_getWhitespaceStrippingPolicy(handle receiver) returns handle = @java:Method {
    name: "getWhitespaceStrippingPolicy",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_load(handle receiver) returns handle = @java:Method {
    name: "load",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_load30(handle receiver) returns handle = @java:Method {
    name: "load30",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltExecutable_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: ["long"]
} external;

function net_sf_saxon_s9api_XsltExecutable_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.XsltExecutable",
    paramTypes: ["long", "int"]
} external;


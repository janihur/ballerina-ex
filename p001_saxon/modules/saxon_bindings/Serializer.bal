import ballerina/jballerina.java;

# Ballerina class mapping for the Java `net.sf.saxon.s9api.Serializer` class.
@java:Binding {'class: "net.sf.saxon.s9api.Serializer"}
public distinct class Serializer {

    *java:JObject;
    *AbstractDestination;

    # The `handle` field that stores the reference to the `net.sf.saxon.s9api.Serializer` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `net.sf.saxon.s9api.Serializer` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `net.sf.saxon.s9api.Serializer` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `close` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function close() returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Serializer_close(self.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `closeAndNotify` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function closeAndNotify() returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Serializer_closeAndNotify(self.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `equals` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return net_sf_saxon_s9api_Serializer_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = net_sf_saxon_s9api_Serializer_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getCombinedOutputProperties` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Properties` value required to map with the Java method parameter.
    # + return - The `Properties` value returning from the Java mapping.
    public function getCombinedOutputProperties(Properties arg0) returns Properties {
        handle externalObj = net_sf_saxon_s9api_Serializer_getCombinedOutputProperties(self.jObj, arg0.jObj);
        Properties newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getContentHandler` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `ContentHandler` or the `SaxonApiException` value returning from the Java mapping.
    public function getContentHandler() returns ContentHandler|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_Serializer_getContentHandler(self.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            ContentHandler newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `getDestinationBaseURI` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `URI` value returning from the Java mapping.
    public function getDestinationBaseURI() returns URI {
        handle externalObj = net_sf_saxon_s9api_Serializer_getDestinationBaseURI(self.jObj);
        URI newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getOutputDestination` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `Object` value returning from the Java mapping.
    public function getOutputDestination() returns Object {
        handle externalObj = net_sf_saxon_s9api_Serializer_getOutputDestination(self.jObj);
        Object newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getOutputProperty` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Property` value required to map with the Java method parameter.
    # + return - The `string` value returning from the Java mapping.
    public function getOutputProperty(Property arg0) returns string? {
        return java:toString(net_sf_saxon_s9api_Serializer_getOutputProperty(self.jObj, arg0.jObj));
    }

    # The function that maps to the `getOutputProperty` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `QName` value required to map with the Java method parameter.
    # + return - The `string` value returning from the Java mapping.
    public function getOutputProperty2(QName arg0) returns string? {
        return java:toString(net_sf_saxon_s9api_Serializer_getOutputProperty2(self.jObj, arg0.jObj));
    }

    # The function that maps to the `getProcessor` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `Processor` value returning from the Java mapping.
    public function getProcessor() returns Processor {
        handle externalObj = net_sf_saxon_s9api_Serializer_getProcessor(self.jObj);
        Processor newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getReceiver` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `PipelineConfiguration` value required to map with the Java method parameter.
    # + arg1 - The `SerializationProperties` value required to map with the Java method parameter.
    # + return - The `Receiver` or the `SaxonApiException` value returning from the Java mapping.
    public function getReceiver(PipelineConfiguration arg0, SerializationProperties arg1) returns Receiver|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_Serializer_getReceiver(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            Receiver newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `getSerializationProperties` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `SerializationProperties` value returning from the Java mapping.
    public function getSerializationProperties() returns SerializationProperties {
        handle externalObj = net_sf_saxon_s9api_Serializer_getSerializationProperties(self.jObj);
        SerializationProperties newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getXMLStreamWriter` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `StreamWriterToReceiver` or the `SaxonApiException` value returning from the Java mapping.
    public function getXMLStreamWriter() returns StreamWriterToReceiver|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_Serializer_getXMLStreamWriter(self.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            StreamWriterToReceiver newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `hashCode` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return net_sf_saxon_s9api_Serializer_hashCode(self.jObj);
    }

    # The function that maps to the `isMustCloseAfterUse` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isMustCloseAfterUse() returns boolean {
        return net_sf_saxon_s9api_Serializer_isMustCloseAfterUse(self.jObj);
    }

    # The function that maps to the `notify` method of `net.sf.saxon.s9api.Serializer`.
    public function notify() {
        net_sf_saxon_s9api_Serializer_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `net.sf.saxon.s9api.Serializer`.
    public function notifyAll() {
        net_sf_saxon_s9api_Serializer_notifyAll(self.jObj);
    }

    # The function that maps to the `onClose` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Action` value required to map with the Java method parameter.
    public function onClose(Action arg0) {
        net_sf_saxon_s9api_Serializer_onClose(self.jObj, arg0.jObj);
    }

    # The function that maps to the `serializeNode` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `XdmNode` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function serializeNode(XdmNode arg0) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Serializer_serializeNode(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `serializeNodeToString` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `XdmNode` value required to map with the Java method parameter.
    # + return - The `string` or the `SaxonApiException` value returning from the Java mapping.
    public function serializeNodeToString(XdmNode arg0) returns string?|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_Serializer_serializeNodeToString(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            return java:toString(externalObj);
        }
    }

    # The function that maps to the `serializeXdmValue` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `XdmValue` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function serializeXdmValue(XdmValue arg0) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Serializer_serializeXdmValue(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `setCharacterMap` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `CharacterMapIndex` value required to map with the Java method parameter.
    public function setCharacterMap(CharacterMapIndex arg0) {
        net_sf_saxon_s9api_Serializer_setCharacterMap(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setCloseOnCompletion` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setCloseOnCompletion(boolean arg0) {
        net_sf_saxon_s9api_Serializer_setCloseOnCompletion(self.jObj, arg0);
    }

    # The function that maps to the `setDestinationBaseURI` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `URI` value required to map with the Java method parameter.
    public function setDestinationBaseURI(URI arg0) {
        net_sf_saxon_s9api_Serializer_setDestinationBaseURI(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setOutputFile` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `File` value required to map with the Java method parameter.
    public function setOutputFile(File arg0) {
        net_sf_saxon_s9api_Serializer_setOutputFile(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setOutputProperties` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Properties` value required to map with the Java method parameter.
    public function setOutputProperties(Properties arg0) {
        net_sf_saxon_s9api_Serializer_setOutputProperties(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setOutputProperties` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `SerializationProperties` value required to map with the Java method parameter.
    public function setOutputProperties2(SerializationProperties arg0) {
        net_sf_saxon_s9api_Serializer_setOutputProperties2(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setOutputProperty` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Property` value required to map with the Java method parameter.
    # + arg1 - The `string` value required to map with the Java method parameter.
    public function setOutputProperty(Property arg0, string arg1) {
        net_sf_saxon_s9api_Serializer_setOutputProperty(self.jObj, arg0.jObj, java:fromString(arg1));
    }

    # The function that maps to the `setOutputProperty` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `QName` value required to map with the Java method parameter.
    # + arg1 - The `string` value required to map with the Java method parameter.
    public function setOutputProperty2(QName arg0, string arg1) {
        net_sf_saxon_s9api_Serializer_setOutputProperty2(self.jObj, arg0.jObj, java:fromString(arg1));
    }

    # The function that maps to the `setOutputStream` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `OutputStream` value required to map with the Java method parameter.
    public function setOutputStream(OutputStream arg0) {
        net_sf_saxon_s9api_Serializer_setOutputStream(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setOutputWriter` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Writer` value required to map with the Java method parameter.
    public function setOutputWriter(Writer arg0) {
        net_sf_saxon_s9api_Serializer_setOutputWriter(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setProcessor` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `Processor` value required to map with the Java method parameter.
    public function setProcessor(Processor arg0) {
        net_sf_saxon_s9api_Serializer_setProcessor(self.jObj, arg0.jObj);
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Serializer_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Serializer_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Serializer`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Serializer_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The function that maps to the `getProperty` method of `net.sf.saxon.s9api.Serializer`.
#
# + arg0 - The `QName` value required to map with the Java method parameter.
# + return - The `Property` value returning from the Java mapping.
public function Serializer_getProperty(QName arg0) returns Property {
    handle externalObj = net_sf_saxon_s9api_Serializer_getProperty(arg0.jObj);
    Property newObj = new (externalObj);
    return newObj;
}

function net_sf_saxon_s9api_Serializer_close(handle receiver) returns error? = @java:Method {
    name: "close",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_closeAndNotify(handle receiver) returns error? = @java:Method {
    name: "closeAndNotify",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["java.lang.Object"]
} external;

function net_sf_saxon_s9api_Serializer_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_getCombinedOutputProperties(handle receiver, handle arg0) returns handle = @java:Method {
    name: "getCombinedOutputProperties",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["java.util.Properties"]
} external;

function net_sf_saxon_s9api_Serializer_getContentHandler(handle receiver) returns handle|error = @java:Method {
    name: "getContentHandler",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_getDestinationBaseURI(handle receiver) returns handle = @java:Method {
    name: "getDestinationBaseURI",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_getOutputDestination(handle receiver) returns handle = @java:Method {
    name: "getOutputDestination",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_getOutputProperty(handle receiver, handle arg0) returns handle = @java:Method {
    name: "getOutputProperty",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.Serializer$Property"]
} external;

function net_sf_saxon_s9api_Serializer_getOutputProperty2(handle receiver, handle arg0) returns handle = @java:Method {
    name: "getOutputProperty",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.QName"]
} external;

function net_sf_saxon_s9api_Serializer_getProcessor(handle receiver) returns handle = @java:Method {
    name: "getProcessor",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_getProperty(handle arg0) returns handle = @java:Method {
    name: "getProperty",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.QName"]
} external;

function net_sf_saxon_s9api_Serializer_getReceiver(handle receiver, handle arg0, handle arg1) returns handle|error = @java:Method {
    name: "getReceiver",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.event.PipelineConfiguration", "net.sf.saxon.serialize.SerializationProperties"]
} external;

function net_sf_saxon_s9api_Serializer_getSerializationProperties(handle receiver) returns handle = @java:Method {
    name: "getSerializationProperties",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_getXMLStreamWriter(handle receiver) returns handle|error = @java:Method {
    name: "getXMLStreamWriter",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_isMustCloseAfterUse(handle receiver) returns boolean = @java:Method {
    name: "isMustCloseAfterUse",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_onClose(handle receiver, handle arg0) = @java:Method {
    name: "onClose",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.Action"]
} external;

function net_sf_saxon_s9api_Serializer_serializeNode(handle receiver, handle arg0) returns error? = @java:Method {
    name: "serializeNode",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.XdmNode"]
} external;

function net_sf_saxon_s9api_Serializer_serializeNodeToString(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "serializeNodeToString",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.XdmNode"]
} external;

function net_sf_saxon_s9api_Serializer_serializeXdmValue(handle receiver, handle arg0) returns error? = @java:Method {
    name: "serializeXdmValue",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.XdmValue"]
} external;

function net_sf_saxon_s9api_Serializer_setCharacterMap(handle receiver, handle arg0) = @java:Method {
    name: "setCharacterMap",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.serialize.CharacterMapIndex"]
} external;

function net_sf_saxon_s9api_Serializer_setCloseOnCompletion(handle receiver, boolean arg0) = @java:Method {
    name: "setCloseOnCompletion",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_Serializer_setDestinationBaseURI(handle receiver, handle arg0) = @java:Method {
    name: "setDestinationBaseURI",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["java.net.URI"]
} external;

function net_sf_saxon_s9api_Serializer_setOutputFile(handle receiver, handle arg0) = @java:Method {
    name: "setOutputFile",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["java.io.File"]
} external;

function net_sf_saxon_s9api_Serializer_setOutputProperties(handle receiver, handle arg0) = @java:Method {
    name: "setOutputProperties",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["java.util.Properties"]
} external;

function net_sf_saxon_s9api_Serializer_setOutputProperties2(handle receiver, handle arg0) = @java:Method {
    name: "setOutputProperties",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.serialize.SerializationProperties"]
} external;

function net_sf_saxon_s9api_Serializer_setOutputProperty(handle receiver, handle arg0, handle arg1) = @java:Method {
    name: "setOutputProperty",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.Serializer$Property", "java.lang.String"]
} external;

function net_sf_saxon_s9api_Serializer_setOutputProperty2(handle receiver, handle arg0, handle arg1) = @java:Method {
    name: "setOutputProperty",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.QName", "java.lang.String"]
} external;

function net_sf_saxon_s9api_Serializer_setOutputStream(handle receiver, handle arg0) = @java:Method {
    name: "setOutputStream",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["java.io.OutputStream"]
} external;

function net_sf_saxon_s9api_Serializer_setOutputWriter(handle receiver, handle arg0) = @java:Method {
    name: "setOutputWriter",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["java.io.Writer"]
} external;

function net_sf_saxon_s9api_Serializer_setProcessor(handle receiver, handle arg0) = @java:Method {
    name: "setProcessor",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["net.sf.saxon.s9api.Processor"]
} external;

function net_sf_saxon_s9api_Serializer_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Serializer_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["long"]
} external;

function net_sf_saxon_s9api_Serializer_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Serializer",
    paramTypes: ["long", "int"]
} external;


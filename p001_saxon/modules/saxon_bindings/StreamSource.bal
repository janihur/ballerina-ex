import ballerina/jballerina.java;

# Ballerina class mapping for the Java `javax.xml.transform.stream.StreamSource` class.
@java:Binding {'class: "javax.xml.transform.stream.StreamSource"}
public distinct class StreamSource {

    *java:JObject;
    *Object;

    # The `handle` field that stores the reference to the `javax.xml.transform.stream.StreamSource` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `javax.xml.transform.stream.StreamSource` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `javax.xml.transform.stream.StreamSource` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `equals` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return javax_xml_transform_stream_StreamSource_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = javax_xml_transform_stream_StreamSource_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getInputStream` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + return - The `InputStream` value returning from the Java mapping.
    public function getInputStream() returns InputStream {
        handle externalObj = javax_xml_transform_stream_StreamSource_getInputStream(self.jObj);
        InputStream newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getPublicId` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getPublicId() returns string? {
        return java:toString(javax_xml_transform_stream_StreamSource_getPublicId(self.jObj));
    }

    # The function that maps to the `getReader` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + return - The `Reader` value returning from the Java mapping.
    public function getReader() returns Reader {
        handle externalObj = javax_xml_transform_stream_StreamSource_getReader(self.jObj);
        Reader newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getSystemId` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getSystemId() returns string? {
        return java:toString(javax_xml_transform_stream_StreamSource_getSystemId(self.jObj));
    }

    # The function that maps to the `hashCode` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return javax_xml_transform_stream_StreamSource_hashCode(self.jObj);
    }

    # The function that maps to the `notify` method of `javax.xml.transform.stream.StreamSource`.
    public function notify() {
        javax_xml_transform_stream_StreamSource_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `javax.xml.transform.stream.StreamSource`.
    public function notifyAll() {
        javax_xml_transform_stream_StreamSource_notifyAll(self.jObj);
    }

    # The function that maps to the `setInputStream` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `InputStream` value required to map with the Java method parameter.
    public function setInputStream(InputStream arg0) {
        javax_xml_transform_stream_StreamSource_setInputStream(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setPublicId` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    public function setPublicId(string arg0) {
        javax_xml_transform_stream_StreamSource_setPublicId(self.jObj, java:fromString(arg0));
    }

    # The function that maps to the `setReader` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `Reader` value required to map with the Java method parameter.
    public function setReader(Reader arg0) {
        javax_xml_transform_stream_StreamSource_setReader(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setSystemId` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `File` value required to map with the Java method parameter.
    public function setSystemId(File arg0) {
        javax_xml_transform_stream_StreamSource_setSystemId(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setSystemId` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    public function setSystemId2(string arg0) {
        javax_xml_transform_stream_StreamSource_setSystemId2(self.jObj, java:fromString(arg0));
    }

    # The function that maps to the `wait` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = javax_xml_transform_stream_StreamSource_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = javax_xml_transform_stream_StreamSource_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `javax.xml.transform.stream.StreamSource`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = javax_xml_transform_stream_StreamSource_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The constructor function to generate an object of `javax.xml.transform.stream.StreamSource`.
#
# + return - The new `StreamSource` class generated.
public function newStreamSource1() returns StreamSource {
    handle externalObj = javax_xml_transform_stream_StreamSource_newStreamSource1();
    StreamSource newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `javax.xml.transform.stream.StreamSource`.
#
# + arg0 - The `File` value required to map with the Java constructor parameter.
# + return - The new `StreamSource` class generated.
public function newStreamSource2(File arg0) returns StreamSource {
    handle externalObj = javax_xml_transform_stream_StreamSource_newStreamSource2(arg0.jObj);
    StreamSource newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `javax.xml.transform.stream.StreamSource`.
#
# + arg0 - The `InputStream` value required to map with the Java constructor parameter.
# + return - The new `StreamSource` class generated.
public function newStreamSource3(InputStream arg0) returns StreamSource {
    handle externalObj = javax_xml_transform_stream_StreamSource_newStreamSource3(arg0.jObj);
    StreamSource newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `javax.xml.transform.stream.StreamSource`.
#
# + arg0 - The `InputStream` value required to map with the Java constructor parameter.
# + arg1 - The `string` value required to map with the Java constructor parameter.
# + return - The new `StreamSource` class generated.
public function newStreamSource4(InputStream arg0, string arg1) returns StreamSource {
    handle externalObj = javax_xml_transform_stream_StreamSource_newStreamSource4(arg0.jObj, java:fromString(arg1));
    StreamSource newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `javax.xml.transform.stream.StreamSource`.
#
# + arg0 - The `Reader` value required to map with the Java constructor parameter.
# + return - The new `StreamSource` class generated.
public function newStreamSource5(Reader arg0) returns StreamSource {
    handle externalObj = javax_xml_transform_stream_StreamSource_newStreamSource5(arg0.jObj);
    StreamSource newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `javax.xml.transform.stream.StreamSource`.
#
# + arg0 - The `Reader` value required to map with the Java constructor parameter.
# + arg1 - The `string` value required to map with the Java constructor parameter.
# + return - The new `StreamSource` class generated.
public function newStreamSource6(Reader arg0, string arg1) returns StreamSource {
    handle externalObj = javax_xml_transform_stream_StreamSource_newStreamSource6(arg0.jObj, java:fromString(arg1));
    StreamSource newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `javax.xml.transform.stream.StreamSource`.
#
# + arg0 - The `string` value required to map with the Java constructor parameter.
# + return - The new `StreamSource` class generated.
public function newStreamSource7(string arg0) returns StreamSource {
    handle externalObj = javax_xml_transform_stream_StreamSource_newStreamSource7(java:fromString(arg0));
    StreamSource newObj = new (externalObj);
    return newObj;
}

# The function that retrieves the value of the public field `FEATURE`.
#
# + return - The `string` value of the field.
public function StreamSource_getFEATURE() returns string? {
    return java:toString(javax_xml_transform_stream_StreamSource_getFEATURE());
}

function javax_xml_transform_stream_StreamSource_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.lang.Object"]
} external;

function javax_xml_transform_stream_StreamSource_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_getInputStream(handle receiver) returns handle = @java:Method {
    name: "getInputStream",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_getPublicId(handle receiver) returns handle = @java:Method {
    name: "getPublicId",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_getReader(handle receiver) returns handle = @java:Method {
    name: "getReader",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_getSystemId(handle receiver) returns handle = @java:Method {
    name: "getSystemId",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_setInputStream(handle receiver, handle arg0) = @java:Method {
    name: "setInputStream",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.InputStream"]
} external;

function javax_xml_transform_stream_StreamSource_setPublicId(handle receiver, handle arg0) = @java:Method {
    name: "setPublicId",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.lang.String"]
} external;

function javax_xml_transform_stream_StreamSource_setReader(handle receiver, handle arg0) = @java:Method {
    name: "setReader",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.Reader"]
} external;

function javax_xml_transform_stream_StreamSource_setSystemId(handle receiver, handle arg0) = @java:Method {
    name: "setSystemId",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.File"]
} external;

function javax_xml_transform_stream_StreamSource_setSystemId2(handle receiver, handle arg0) = @java:Method {
    name: "setSystemId",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.lang.String"]
} external;

function javax_xml_transform_stream_StreamSource_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["long"]
} external;

function javax_xml_transform_stream_StreamSource_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["long", "int"]
} external;

function javax_xml_transform_stream_StreamSource_getFEATURE() returns handle = @java:FieldGet {
    name: "FEATURE",
    'class: "javax.xml.transform.stream.StreamSource"
} external;

function javax_xml_transform_stream_StreamSource_newStreamSource1() returns handle = @java:Constructor {
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: []
} external;

function javax_xml_transform_stream_StreamSource_newStreamSource2(handle arg0) returns handle = @java:Constructor {
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.File"]
} external;

function javax_xml_transform_stream_StreamSource_newStreamSource3(handle arg0) returns handle = @java:Constructor {
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.InputStream"]
} external;

function javax_xml_transform_stream_StreamSource_newStreamSource4(handle arg0, handle arg1) returns handle = @java:Constructor {
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.InputStream", "java.lang.String"]
} external;

function javax_xml_transform_stream_StreamSource_newStreamSource5(handle arg0) returns handle = @java:Constructor {
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.Reader"]
} external;

function javax_xml_transform_stream_StreamSource_newStreamSource6(handle arg0, handle arg1) returns handle = @java:Constructor {
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.io.Reader", "java.lang.String"]
} external;

function javax_xml_transform_stream_StreamSource_newStreamSource7(handle arg0) returns handle = @java:Constructor {
    'class: "javax.xml.transform.stream.StreamSource",
    paramTypes: ["java.lang.String"]
} external;


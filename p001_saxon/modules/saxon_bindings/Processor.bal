import ballerina/jballerina.java;

# Ballerina class mapping for the Java `net.sf.saxon.s9api.Processor` class.
@java:Binding {'class: "net.sf.saxon.s9api.Processor"}
public distinct class Processor {

    *java:JObject;
    *Object;

    # The `handle` field that stores the reference to the `net.sf.saxon.s9api.Processor` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `net.sf.saxon.s9api.Processor` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `net.sf.saxon.s9api.Processor` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `declareCollation` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + arg1 - The `Comparator` value required to map with the Java method parameter.
    public function declareCollation(string arg0, Comparator arg1) {
        net_sf_saxon_s9api_Processor_declareCollation(self.jObj, java:fromString(arg0), arg1.jObj);
    }

    # The function that maps to the `equals` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return net_sf_saxon_s9api_Processor_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = net_sf_saxon_s9api_Processor_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getConfigurationProperty` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `Feature` value required to map with the Java method parameter.
    # + return - The `Object` value returning from the Java mapping.
    public function getConfigurationProperty(Feature arg0) returns Object {
        handle externalObj = net_sf_saxon_s9api_Processor_getConfigurationProperty(self.jObj, arg0.jObj);
        Object newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getConfigurationProperty` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + return - The `Object` value returning from the Java mapping.
    public function getConfigurationProperty2(string arg0) returns Object {
        handle externalObj = net_sf_saxon_s9api_Processor_getConfigurationProperty2(self.jObj, java:fromString(arg0));
        Object newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getSaxonEdition` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getSaxonEdition() returns string? {
        return java:toString(net_sf_saxon_s9api_Processor_getSaxonEdition(self.jObj));
    }

    # The function that maps to the `getSaxonProductVersion` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getSaxonProductVersion() returns string? {
        return java:toString(net_sf_saxon_s9api_Processor_getSaxonProductVersion(self.jObj));
    }

    # The function that maps to the `getSchemaManager` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `SchemaManager` value returning from the Java mapping.
    public function getSchemaManager() returns SchemaManager {
        handle externalObj = net_sf_saxon_s9api_Processor_getSchemaManager(self.jObj);
        SchemaManager newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getUnderlyingConfiguration` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `Configuration` value returning from the Java mapping.
    public function getUnderlyingConfiguration() returns Configuration {
        handle externalObj = net_sf_saxon_s9api_Processor_getUnderlyingConfiguration(self.jObj);
        Configuration newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getXmlVersion` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getXmlVersion() returns string? {
        return java:toString(net_sf_saxon_s9api_Processor_getXmlVersion(self.jObj));
    }

    # The function that maps to the `hashCode` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return net_sf_saxon_s9api_Processor_hashCode(self.jObj);
    }

    # The function that maps to the `isSchemaAware` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isSchemaAware() returns boolean {
        return net_sf_saxon_s9api_Processor_isSchemaAware(self.jObj);
    }

    # The function that maps to the `newDocumentBuilder` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `DocumentBuilder` value returning from the Java mapping.
    public function newDocumentBuilder() returns DocumentBuilder {
        handle externalObj = net_sf_saxon_s9api_Processor_newDocumentBuilder(self.jObj);
        DocumentBuilder newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer() returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Processor_newSerializer(self.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `File` value required to map with the Java method parameter.
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer2(File arg0) returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Processor_newSerializer2(self.jObj, arg0.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `OutputStream` value required to map with the Java method parameter.
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer3(OutputStream arg0) returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Processor_newSerializer3(self.jObj, arg0.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newSerializer` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `Writer` value required to map with the Java method parameter.
    # + return - The `Serializer` value returning from the Java mapping.
    public function newSerializer4(Writer arg0) returns Serializer {
        handle externalObj = net_sf_saxon_s9api_Processor_newSerializer4(self.jObj, arg0.jObj);
        Serializer newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newXPathCompiler` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `XPathCompiler` value returning from the Java mapping.
    public function newXPathCompiler() returns XPathCompiler {
        handle externalObj = net_sf_saxon_s9api_Processor_newXPathCompiler(self.jObj);
        XPathCompiler newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newXQueryCompiler` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `XQueryCompiler` value returning from the Java mapping.
    public function newXQueryCompiler() returns XQueryCompiler {
        handle externalObj = net_sf_saxon_s9api_Processor_newXQueryCompiler(self.jObj);
        XQueryCompiler newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `newXsltCompiler` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `XsltCompiler` value returning from the Java mapping.
    public function newXsltCompiler() returns XsltCompiler {
        handle externalObj = net_sf_saxon_s9api_Processor_newXsltCompiler(self.jObj);
        XsltCompiler newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `notify` method of `net.sf.saxon.s9api.Processor`.
    public function notify() {
        net_sf_saxon_s9api_Processor_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `net.sf.saxon.s9api.Processor`.
    public function notifyAll() {
        net_sf_saxon_s9api_Processor_notifyAll(self.jObj);
    }

    # The function that maps to the `registerExtensionFunction` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `ExtensionFunction` value required to map with the Java method parameter.
    public function registerExtensionFunction(ExtensionFunction arg0) {
        net_sf_saxon_s9api_Processor_registerExtensionFunction(self.jObj, arg0.jObj);
    }

    # The function that maps to the `registerExtensionFunction` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `ExtensionFunctionDefinition` value required to map with the Java method parameter.
    public function registerExtensionFunction2(ExtensionFunctionDefinition arg0) {
        net_sf_saxon_s9api_Processor_registerExtensionFunction2(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setConfigurationProperty` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `Feature` value required to map with the Java method parameter.
    # + arg1 - The `Object` value required to map with the Java method parameter.
    public function setConfigurationProperty(Feature arg0, Object arg1) {
        net_sf_saxon_s9api_Processor_setConfigurationProperty(self.jObj, arg0.jObj, arg1.jObj);
    }

    # The function that maps to the `setConfigurationProperty` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + arg1 - The `Object` value required to map with the Java method parameter.
    public function setConfigurationProperty2(string arg0, Object arg1) {
        net_sf_saxon_s9api_Processor_setConfigurationProperty2(self.jObj, java:fromString(arg0), arg1.jObj);
    }

    # The function that maps to the `setXmlVersion` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    public function setXmlVersion(string arg0) {
        net_sf_saxon_s9api_Processor_setXmlVersion(self.jObj, java:fromString(arg0));
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Processor`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Processor_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Processor_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_Processor_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `writeXdmValue` method of `net.sf.saxon.s9api.Processor`.
    #
    # + arg0 - The `XdmValue` value required to map with the Java method parameter.
    # + arg1 - The `Destination` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function writeXdmValue(XdmValue arg0, Destination arg1) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_Processor_writeXdmValue(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The constructor function to generate an object of `net.sf.saxon.s9api.Processor`.
#
# + arg0 - The `boolean` value required to map with the Java constructor parameter.
# + return - The new `Processor` class generated.
public function newProcessor1(boolean arg0) returns Processor {
    handle externalObj = net_sf_saxon_s9api_Processor_newProcessor1(arg0);
    Processor newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `net.sf.saxon.s9api.Processor`.
#
# + arg0 - The `Configuration` value required to map with the Java constructor parameter.
# + return - The new `Processor` class generated.
public function newProcessor2(Configuration arg0) returns Processor {
    handle externalObj = net_sf_saxon_s9api_Processor_newProcessor2(arg0.jObj);
    Processor newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `net.sf.saxon.s9api.Processor`.
#
# + arg0 - The `Source` value required to map with the Java constructor parameter.
# + return - The new `Processor` class or `SaxonApiException` error generated.
public function newProcessor3(Source arg0) returns Processor|SaxonApiException {
    handle|error externalObj = net_sf_saxon_s9api_Processor_newProcessor3(arg0.jObj);
    if (externalObj is error) {
        SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
        return e;
    } else {
        Processor newObj = new (externalObj);
        return newObj;
    }
}

function net_sf_saxon_s9api_Processor_declareCollation(handle receiver, handle arg0, handle arg1) = @java:Method {
    name: "declareCollation",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.lang.String", "java.util.Comparator"]
} external;

function net_sf_saxon_s9api_Processor_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.lang.Object"]
} external;

function net_sf_saxon_s9api_Processor_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_getConfigurationProperty(handle receiver, handle arg0) returns handle = @java:Method {
    name: "getConfigurationProperty",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["net.sf.saxon.lib.Feature"]
} external;

function net_sf_saxon_s9api_Processor_getConfigurationProperty2(handle receiver, handle arg0) returns handle = @java:Method {
    name: "getConfigurationProperty",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.lang.String"]
} external;

function net_sf_saxon_s9api_Processor_getSaxonEdition(handle receiver) returns handle = @java:Method {
    name: "getSaxonEdition",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_getSaxonProductVersion(handle receiver) returns handle = @java:Method {
    name: "getSaxonProductVersion",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_getSchemaManager(handle receiver) returns handle = @java:Method {
    name: "getSchemaManager",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_getUnderlyingConfiguration(handle receiver) returns handle = @java:Method {
    name: "getUnderlyingConfiguration",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_getXmlVersion(handle receiver) returns handle = @java:Method {
    name: "getXmlVersion",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_isSchemaAware(handle receiver) returns boolean = @java:Method {
    name: "isSchemaAware",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_newDocumentBuilder(handle receiver) returns handle = @java:Method {
    name: "newDocumentBuilder",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_newSerializer(handle receiver) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_newSerializer2(handle receiver, handle arg0) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.io.File"]
} external;

function net_sf_saxon_s9api_Processor_newSerializer3(handle receiver, handle arg0) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.io.OutputStream"]
} external;

function net_sf_saxon_s9api_Processor_newSerializer4(handle receiver, handle arg0) returns handle = @java:Method {
    name: "newSerializer",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.io.Writer"]
} external;

function net_sf_saxon_s9api_Processor_newXPathCompiler(handle receiver) returns handle = @java:Method {
    name: "newXPathCompiler",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_newXQueryCompiler(handle receiver) returns handle = @java:Method {
    name: "newXQueryCompiler",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_newXsltCompiler(handle receiver) returns handle = @java:Method {
    name: "newXsltCompiler",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_registerExtensionFunction(handle receiver, handle arg0) = @java:Method {
    name: "registerExtensionFunction",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["net.sf.saxon.s9api.ExtensionFunction"]
} external;

function net_sf_saxon_s9api_Processor_registerExtensionFunction2(handle receiver, handle arg0) = @java:Method {
    name: "registerExtensionFunction",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["net.sf.saxon.lib.ExtensionFunctionDefinition"]
} external;

function net_sf_saxon_s9api_Processor_setConfigurationProperty(handle receiver, handle arg0, handle arg1) = @java:Method {
    name: "setConfigurationProperty",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["net.sf.saxon.lib.Feature", "java.lang.Object"]
} external;

function net_sf_saxon_s9api_Processor_setConfigurationProperty2(handle receiver, handle arg0, handle arg1) = @java:Method {
    name: "setConfigurationProperty",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.lang.String", "java.lang.Object"]
} external;

function net_sf_saxon_s9api_Processor_setXmlVersion(handle receiver, handle arg0) = @java:Method {
    name: "setXmlVersion",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["java.lang.String"]
} external;

function net_sf_saxon_s9api_Processor_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: []
} external;

function net_sf_saxon_s9api_Processor_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["long"]
} external;

function net_sf_saxon_s9api_Processor_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["long", "int"]
} external;

function net_sf_saxon_s9api_Processor_writeXdmValue(handle receiver, handle arg0, handle arg1) returns error? = @java:Method {
    name: "writeXdmValue",
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["net.sf.saxon.s9api.XdmValue", "net.sf.saxon.s9api.Destination"]
} external;

function net_sf_saxon_s9api_Processor_newProcessor1(boolean arg0) returns handle = @java:Constructor {
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_Processor_newProcessor2(handle arg0) returns handle = @java:Constructor {
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["net.sf.saxon.Configuration"]
} external;

function net_sf_saxon_s9api_Processor_newProcessor3(handle arg0) returns handle|error = @java:Constructor {
    'class: "net.sf.saxon.s9api.Processor",
    paramTypes: ["javax.xml.transform.Source"]
} external;


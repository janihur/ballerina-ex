// Ballerina error type for `net.sf.saxon.s9api.SaxonApiException`.

public const SAXONAPIEXCEPTION = "SaxonApiException";

type SaxonApiExceptionData record {
    string message;
};

public type SaxonApiException distinct error<SaxonApiExceptionData>;


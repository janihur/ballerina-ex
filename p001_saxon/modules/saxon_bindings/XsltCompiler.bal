import ballerina/jballerina.java;

# Ballerina class mapping for the Java `net.sf.saxon.s9api.XsltCompiler` class.
@java:Binding {'class: "net.sf.saxon.s9api.XsltCompiler"}
public distinct class XsltCompiler {

    *java:JObject;
    *Object;

    # The `handle` field that stores the reference to the `net.sf.saxon.s9api.XsltCompiler` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `net.sf.saxon.s9api.XsltCompiler` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `net.sf.saxon.s9api.XsltCompiler` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `addCompilePackages` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `Iterable` value required to map with the Java method parameter.
    # + arg1 - The `boolean` value required to map with the Java method parameter.
    # + return - The `Iterable` value returning from the Java mapping.
    public function addCompilePackages(Iterable arg0, boolean arg1) returns Iterable {
        handle externalObj = net_sf_saxon_s9api_XsltCompiler_addCompilePackages(self.jObj, arg0.jObj, arg1);
        Iterable newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `clearParameters` method of `net.sf.saxon.s9api.XsltCompiler`.
    public function clearParameters() {
        net_sf_saxon_s9api_XsltCompiler_clearParameters(self.jObj);
    }

    # The function that maps to the `compile` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `Source` value required to map with the Java method parameter.
    # + return - The `XsltExecutable` or the `SaxonApiException` value returning from the Java mapping.
    public function compile(Source arg0) returns XsltExecutable|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_compile(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XsltExecutable newObj = new (externalObj);
            return newObj;
        }
    }

    public function compile2(StreamSource arg0) returns XsltExecutable|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_compile2(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XsltExecutable newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `compilePackage` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `Source` value required to map with the Java method parameter.
    # + return - The `XsltPackage` or the `SaxonApiException` value returning from the Java mapping.
    public function compilePackage(Source arg0) returns XsltPackage|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_compilePackage(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XsltPackage newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `compilePackages` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `Iterable` value required to map with the Java method parameter.
    # + return - The `Iterable` value returning from the Java mapping.
    public function compilePackages(Iterable arg0) returns Iterable {
        handle externalObj = net_sf_saxon_s9api_XsltCompiler_compilePackages(self.jObj, arg0.jObj);
        Iterable newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `declareDefaultCollation` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    public function declareDefaultCollation(string arg0) {
        net_sf_saxon_s9api_XsltCompiler_declareDefaultCollation(self.jObj, java:fromString(arg0));
    }

    # The function that maps to the `equals` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getAssociatedStylesheet` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `Source` value required to map with the Java method parameter.
    # + arg1 - The `string` value required to map with the Java method parameter.
    # + arg2 - The `string` value required to map with the Java method parameter.
    # + arg3 - The `string` value required to map with the Java method parameter.
    # + return - The `Source` or the `SaxonApiException` value returning from the Java mapping.
    public function getAssociatedStylesheet(Source arg0, string arg1, string arg2, string arg3) returns Source|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_getAssociatedStylesheet(self.jObj, arg0.jObj, java:fromString(arg1), java:fromString(arg2), java:fromString(arg3));
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            Source newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `getClass` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = net_sf_saxon_s9api_XsltCompiler_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getDefaultCollation` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getDefaultCollation() returns string? {
        return java:toString(net_sf_saxon_s9api_XsltCompiler_getDefaultCollation(self.jObj));
    }

    # The function that maps to the `getErrorListener` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `ErrorListener` value returning from the Java mapping.
    public function getErrorListener() returns ErrorListener {
        handle externalObj = net_sf_saxon_s9api_XsltCompiler_getErrorListener(self.jObj);
        ErrorListener newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getProcessor` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `Processor` value returning from the Java mapping.
    public function getProcessor() returns Processor {
        handle externalObj = net_sf_saxon_s9api_XsltCompiler_getProcessor(self.jObj);
        Processor newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getTargetEdition` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getTargetEdition() returns string? {
        return java:toString(net_sf_saxon_s9api_XsltCompiler_getTargetEdition(self.jObj));
    }

    # The function that maps to the `getURIResolver` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `URIResolver` value returning from the Java mapping.
    public function getURIResolver() returns URIResolver {
        handle externalObj = net_sf_saxon_s9api_XsltCompiler_getURIResolver(self.jObj);
        URIResolver newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getUnderlyingCompilerInfo` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `CompilerInfo` value returning from the Java mapping.
    public function getUnderlyingCompilerInfo() returns CompilerInfo {
        handle externalObj = net_sf_saxon_s9api_XsltCompiler_getUnderlyingCompilerInfo(self.jObj);
        CompilerInfo newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getXsltLanguageVersion` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getXsltLanguageVersion() returns string? {
        return java:toString(net_sf_saxon_s9api_XsltCompiler_getXsltLanguageVersion(self.jObj));
    }

    # The function that maps to the `hashCode` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return net_sf_saxon_s9api_XsltCompiler_hashCode(self.jObj);
    }

    # The function that maps to the `importPackage` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `XsltPackage` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function importPackage(XsltPackage arg0) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_XsltCompiler_importPackage(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `importPackage` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `XsltPackage` value required to map with the Java method parameter.
    # + arg1 - The `string` value required to map with the Java method parameter.
    # + arg2 - The `string` value required to map with the Java method parameter.
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function importPackage2(XsltPackage arg0, string arg1, string arg2) returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_XsltCompiler_importPackage2(self.jObj, arg0.jObj, java:fromString(arg1), java:fromString(arg2));
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `importXQueryEnvironment` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `XQueryCompiler` value required to map with the Java method parameter.
    public function importXQueryEnvironment(XQueryCompiler arg0) {
        net_sf_saxon_s9api_XsltCompiler_importXQueryEnvironment(self.jObj, arg0.jObj);
    }

    # The function that maps to the `isAssertionsEnabled` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isAssertionsEnabled() returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_isAssertionsEnabled(self.jObj);
    }

    # The function that maps to the `isCompileWithTracing` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isCompileWithTracing() returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_isCompileWithTracing(self.jObj);
    }

    # The function that maps to the `isFastCompilation` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isFastCompilation() returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_isFastCompilation(self.jObj);
    }

    # The function that maps to the `isGenerateByteCode` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isGenerateByteCode() returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_isGenerateByteCode(self.jObj);
    }

    # The function that maps to the `isJustInTimeCompilation` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isJustInTimeCompilation() returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_isJustInTimeCompilation(self.jObj);
    }

    # The function that maps to the `isRelocatable` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isRelocatable() returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_isRelocatable(self.jObj);
    }

    # The function that maps to the `isSchemaAware` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `boolean` value returning from the Java mapping.
    public function isSchemaAware() returns boolean {
        return net_sf_saxon_s9api_XsltCompiler_isSchemaAware(self.jObj);
    }

    # The function that maps to the `loadExecutablePackage` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `URI` value required to map with the Java method parameter.
    # + return - The `XsltExecutable` or the `SaxonApiException` value returning from the Java mapping.
    public function loadExecutablePackage(URI arg0) returns XsltExecutable|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_loadExecutablePackage(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XsltExecutable newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `loadLibraryPackage` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `URI` value required to map with the Java method parameter.
    # + return - The `XsltPackage` or the `SaxonApiException` value returning from the Java mapping.
    public function loadLibraryPackage(URI arg0) returns XsltPackage|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_loadLibraryPackage(self.jObj, arg0.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XsltPackage newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `notify` method of `net.sf.saxon.s9api.XsltCompiler`.
    public function notify() {
        net_sf_saxon_s9api_XsltCompiler_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `net.sf.saxon.s9api.XsltCompiler`.
    public function notifyAll() {
        net_sf_saxon_s9api_XsltCompiler_notifyAll(self.jObj);
    }

    # The function that maps to the `obtainPackage` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + arg1 - The `string` value required to map with the Java method parameter.
    # + return - The `XsltPackage` or the `SaxonApiException` value returning from the Java mapping.
    public function obtainPackage(string arg0, string arg1) returns XsltPackage|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_obtainPackage(self.jObj, java:fromString(arg0), java:fromString(arg1));
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XsltPackage newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `obtainPackageWithAlias` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    # + return - The `XsltPackage` or the `SaxonApiException` value returning from the Java mapping.
    public function obtainPackageWithAlias(string arg0) returns XsltPackage|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_XsltCompiler_obtainPackageWithAlias(self.jObj, java:fromString(arg0));
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            XsltPackage newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `setAssertionsEnabled` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setAssertionsEnabled(boolean arg0) {
        net_sf_saxon_s9api_XsltCompiler_setAssertionsEnabled(self.jObj, arg0);
    }

    # The function that maps to the `setCompileWithTracing` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setCompileWithTracing(boolean arg0) {
        net_sf_saxon_s9api_XsltCompiler_setCompileWithTracing(self.jObj, arg0);
    }

    # The function that maps to the `setErrorList` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `List` value required to map with the Java method parameter.
    public function setErrorList(List arg0) {
        net_sf_saxon_s9api_XsltCompiler_setErrorList(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setErrorListener` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `ErrorListener` value required to map with the Java method parameter.
    public function setErrorListener(ErrorListener arg0) {
        net_sf_saxon_s9api_XsltCompiler_setErrorListener(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setFastCompilation` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setFastCompilation(boolean arg0) {
        net_sf_saxon_s9api_XsltCompiler_setFastCompilation(self.jObj, arg0);
    }

    # The function that maps to the `setGenerateByteCode` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setGenerateByteCode(boolean arg0) {
        net_sf_saxon_s9api_XsltCompiler_setGenerateByteCode(self.jObj, arg0);
    }

    # The function that maps to the `setJustInTimeCompilation` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setJustInTimeCompilation(boolean arg0) {
        net_sf_saxon_s9api_XsltCompiler_setJustInTimeCompilation(self.jObj, arg0);
    }

    # The function that maps to the `setParameter` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `QName` value required to map with the Java method parameter.
    # + arg1 - The `XdmValue` value required to map with the Java method parameter.
    public function setParameter(QName arg0, XdmValue arg1) {
        net_sf_saxon_s9api_XsltCompiler_setParameter(self.jObj, arg0.jObj, arg1.jObj);
    }

    # The function that maps to the `setRelocatable` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setRelocatable(boolean arg0) {
        net_sf_saxon_s9api_XsltCompiler_setRelocatable(self.jObj, arg0);
    }

    # The function that maps to the `setSchemaAware` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `boolean` value required to map with the Java method parameter.
    public function setSchemaAware(boolean arg0) {
        net_sf_saxon_s9api_XsltCompiler_setSchemaAware(self.jObj, arg0);
    }

    # The function that maps to the `setTargetEdition` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    public function setTargetEdition(string arg0) {
        net_sf_saxon_s9api_XsltCompiler_setTargetEdition(self.jObj, java:fromString(arg0));
    }

    # The function that maps to the `setURIResolver` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `URIResolver` value required to map with the Java method parameter.
    public function setURIResolver(URIResolver arg0) {
        net_sf_saxon_s9api_XsltCompiler_setURIResolver(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setXsltLanguageVersion` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `string` value required to map with the Java method parameter.
    public function setXsltLanguageVersion(string arg0) {
        net_sf_saxon_s9api_XsltCompiler_setXsltLanguageVersion(self.jObj, java:fromString(arg0));
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_XsltCompiler_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_XsltCompiler_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.XsltCompiler`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_XsltCompiler_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

function net_sf_saxon_s9api_XsltCompiler_addCompilePackages(handle receiver, handle arg0, boolean arg1) returns handle = @java:Method {
    name: "addCompilePackages",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.Iterable", "boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_clearParameters(handle receiver) = @java:Method {
    name: "clearParameters",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_compile(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "compile",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["javax.xml.transform.Source"]
} external;

function net_sf_saxon_s9api_XsltCompiler_compile2(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "compile",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["javax.xml.transform.stream.StreamSource"]
} external;

function net_sf_saxon_s9api_XsltCompiler_compilePackage(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "compilePackage",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["javax.xml.transform.Source"]
} external;

function net_sf_saxon_s9api_XsltCompiler_compilePackages(handle receiver, handle arg0) returns handle = @java:Method {
    name: "compilePackages",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.Iterable"]
} external;

function net_sf_saxon_s9api_XsltCompiler_declareDefaultCollation(handle receiver, handle arg0) = @java:Method {
    name: "declareDefaultCollation",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltCompiler_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.Object"]
} external;

function net_sf_saxon_s9api_XsltCompiler_getAssociatedStylesheet(handle receiver, handle arg0, handle arg1, handle arg2, handle arg3) returns handle|error = @java:Method {
    name: "getAssociatedStylesheet",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["javax.xml.transform.Source", "java.lang.String", "java.lang.String", "java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltCompiler_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_getDefaultCollation(handle receiver) returns handle = @java:Method {
    name: "getDefaultCollation",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_getErrorListener(handle receiver) returns handle = @java:Method {
    name: "getErrorListener",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_getProcessor(handle receiver) returns handle = @java:Method {
    name: "getProcessor",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_getTargetEdition(handle receiver) returns handle = @java:Method {
    name: "getTargetEdition",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_getURIResolver(handle receiver) returns handle = @java:Method {
    name: "getURIResolver",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_getUnderlyingCompilerInfo(handle receiver) returns handle = @java:Method {
    name: "getUnderlyingCompilerInfo",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_getXsltLanguageVersion(handle receiver) returns handle = @java:Method {
    name: "getXsltLanguageVersion",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_importPackage(handle receiver, handle arg0) returns error? = @java:Method {
    name: "importPackage",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["net.sf.saxon.s9api.XsltPackage"]
} external;

function net_sf_saxon_s9api_XsltCompiler_importPackage2(handle receiver, handle arg0, handle arg1, handle arg2) returns error? = @java:Method {
    name: "importPackage",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["net.sf.saxon.s9api.XsltPackage", "java.lang.String", "java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltCompiler_importXQueryEnvironment(handle receiver, handle arg0) = @java:Method {
    name: "importXQueryEnvironment",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["net.sf.saxon.s9api.XQueryCompiler"]
} external;

function net_sf_saxon_s9api_XsltCompiler_isAssertionsEnabled(handle receiver) returns boolean = @java:Method {
    name: "isAssertionsEnabled",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_isCompileWithTracing(handle receiver) returns boolean = @java:Method {
    name: "isCompileWithTracing",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_isFastCompilation(handle receiver) returns boolean = @java:Method {
    name: "isFastCompilation",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_isGenerateByteCode(handle receiver) returns boolean = @java:Method {
    name: "isGenerateByteCode",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_isJustInTimeCompilation(handle receiver) returns boolean = @java:Method {
    name: "isJustInTimeCompilation",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_isRelocatable(handle receiver) returns boolean = @java:Method {
    name: "isRelocatable",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_isSchemaAware(handle receiver) returns boolean = @java:Method {
    name: "isSchemaAware",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_loadExecutablePackage(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "loadExecutablePackage",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.net.URI"]
} external;

function net_sf_saxon_s9api_XsltCompiler_loadLibraryPackage(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "loadLibraryPackage",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.net.URI"]
} external;

function net_sf_saxon_s9api_XsltCompiler_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_obtainPackage(handle receiver, handle arg0, handle arg1) returns handle|error = @java:Method {
    name: "obtainPackage",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.String", "java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltCompiler_obtainPackageWithAlias(handle receiver, handle arg0) returns handle|error = @java:Method {
    name: "obtainPackageWithAlias",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setAssertionsEnabled(handle receiver, boolean arg0) = @java:Method {
    name: "setAssertionsEnabled",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setCompileWithTracing(handle receiver, boolean arg0) = @java:Method {
    name: "setCompileWithTracing",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setErrorList(handle receiver, handle arg0) = @java:Method {
    name: "setErrorList",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.util.List"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setErrorListener(handle receiver, handle arg0) = @java:Method {
    name: "setErrorListener",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["javax.xml.transform.ErrorListener"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setFastCompilation(handle receiver, boolean arg0) = @java:Method {
    name: "setFastCompilation",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setGenerateByteCode(handle receiver, boolean arg0) = @java:Method {
    name: "setGenerateByteCode",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setJustInTimeCompilation(handle receiver, boolean arg0) = @java:Method {
    name: "setJustInTimeCompilation",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setParameter(handle receiver, handle arg0, handle arg1) = @java:Method {
    name: "setParameter",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["net.sf.saxon.s9api.QName", "net.sf.saxon.s9api.XdmValue"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setRelocatable(handle receiver, boolean arg0) = @java:Method {
    name: "setRelocatable",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setSchemaAware(handle receiver, boolean arg0) = @java:Method {
    name: "setSchemaAware",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["boolean"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setTargetEdition(handle receiver, handle arg0) = @java:Method {
    name: "setTargetEdition",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setURIResolver(handle receiver, handle arg0) = @java:Method {
    name: "setURIResolver",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["javax.xml.transform.URIResolver"]
} external;

function net_sf_saxon_s9api_XsltCompiler_setXsltLanguageVersion(handle receiver, handle arg0) = @java:Method {
    name: "setXsltLanguageVersion",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["java.lang.String"]
} external;

function net_sf_saxon_s9api_XsltCompiler_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: []
} external;

function net_sf_saxon_s9api_XsltCompiler_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["long"]
} external;

function net_sf_saxon_s9api_XsltCompiler_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.XsltCompiler",
    paramTypes: ["long", "int"]
} external;


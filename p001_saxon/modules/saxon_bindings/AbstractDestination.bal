import ballerina/jballerina.java;

# Ballerina class mapping for the Java `net.sf.saxon.s9api.AbstractDestination` class.
@java:Binding {'class: "net.sf.saxon.s9api.AbstractDestination"}
public distinct class AbstractDestination {

    *java:JObject;
    *Object;

    # The `handle` field that stores the reference to the `net.sf.saxon.s9api.AbstractDestination` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `net.sf.saxon.s9api.AbstractDestination` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `net.sf.saxon.s9api.AbstractDestination` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `close` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function close() returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_AbstractDestination_close(self.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `closeAndNotify` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + return - The `SaxonApiException` value returning from the Java mapping.
    public function closeAndNotify() returns SaxonApiException? {
        error|() externalObj = net_sf_saxon_s9api_AbstractDestination_closeAndNotify(self.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `equals` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return net_sf_saxon_s9api_AbstractDestination_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `getClass` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = net_sf_saxon_s9api_AbstractDestination_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getDestinationBaseURI` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + return - The `URI` value returning from the Java mapping.
    public function getDestinationBaseURI() returns URI {
        handle externalObj = net_sf_saxon_s9api_AbstractDestination_getDestinationBaseURI(self.jObj);
        URI newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getReceiver` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + arg0 - The `PipelineConfiguration` value required to map with the Java method parameter.
    # + arg1 - The `SerializationProperties` value required to map with the Java method parameter.
    # + return - The `Receiver` or the `SaxonApiException` value returning from the Java mapping.
    public function getReceiver(PipelineConfiguration arg0, SerializationProperties arg1) returns Receiver|SaxonApiException {
        handle|error externalObj = net_sf_saxon_s9api_AbstractDestination_getReceiver(self.jObj, arg0.jObj, arg1.jObj);
        if (externalObj is error) {
            SaxonApiException e = error SaxonApiException(SAXONAPIEXCEPTION, externalObj, message = externalObj.message());
            return e;
        } else {
            Receiver newObj = new (externalObj);
            return newObj;
        }
    }

    # The function that maps to the `hashCode` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return net_sf_saxon_s9api_AbstractDestination_hashCode(self.jObj);
    }

    # The function that maps to the `notify` method of `net.sf.saxon.s9api.AbstractDestination`.
    public function notify() {
        net_sf_saxon_s9api_AbstractDestination_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `net.sf.saxon.s9api.AbstractDestination`.
    public function notifyAll() {
        net_sf_saxon_s9api_AbstractDestination_notifyAll(self.jObj);
    }

    # The function that maps to the `onClose` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + arg0 - The `Action` value required to map with the Java method parameter.
    public function onClose(Action arg0) {
        net_sf_saxon_s9api_AbstractDestination_onClose(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setDestinationBaseURI` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + arg0 - The `URI` value required to map with the Java method parameter.
    public function setDestinationBaseURI(URI arg0) {
        net_sf_saxon_s9api_AbstractDestination_setDestinationBaseURI(self.jObj, arg0.jObj);
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_AbstractDestination_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_AbstractDestination_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.AbstractDestination`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_AbstractDestination_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The constructor function to generate an object of `net.sf.saxon.s9api.AbstractDestination`.
#
# + return - The new `AbstractDestination` class generated.
public function newAbstractDestination1() returns AbstractDestination {
    handle externalObj = net_sf_saxon_s9api_AbstractDestination_newAbstractDestination1();
    AbstractDestination newObj = new (externalObj);
    return newObj;
}

function net_sf_saxon_s9api_AbstractDestination_close(handle receiver) returns error? = @java:Method {
    name: "close",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_closeAndNotify(handle receiver) returns error? = @java:Method {
    name: "closeAndNotify",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: ["java.lang.Object"]
} external;

function net_sf_saxon_s9api_AbstractDestination_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_getDestinationBaseURI(handle receiver) returns handle = @java:Method {
    name: "getDestinationBaseURI",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_getReceiver(handle receiver, handle arg0, handle arg1) returns handle|error = @java:Method {
    name: "getReceiver",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: ["net.sf.saxon.event.PipelineConfiguration", "net.sf.saxon.serialize.SerializationProperties"]
} external;

function net_sf_saxon_s9api_AbstractDestination_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_onClose(handle receiver, handle arg0) = @java:Method {
    name: "onClose",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: ["net.sf.saxon.s9api.Action"]
} external;

function net_sf_saxon_s9api_AbstractDestination_setDestinationBaseURI(handle receiver, handle arg0) = @java:Method {
    name: "setDestinationBaseURI",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: ["java.net.URI"]
} external;

function net_sf_saxon_s9api_AbstractDestination_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;

function net_sf_saxon_s9api_AbstractDestination_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: ["long"]
} external;

function net_sf_saxon_s9api_AbstractDestination_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: ["long", "int"]
} external;

function net_sf_saxon_s9api_AbstractDestination_newAbstractDestination1() returns handle = @java:Constructor {
    'class: "net.sf.saxon.s9api.AbstractDestination",
    paramTypes: []
} external;


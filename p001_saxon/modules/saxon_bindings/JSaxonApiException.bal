import ballerina/jballerina.java;
import ballerina/jballerina.java.arrays as jarrays;

# Ballerina class mapping for the Java `net.sf.saxon.s9api.SaxonApiException` class.
@java:Binding {'class: "net.sf.saxon.s9api.SaxonApiException"}
public distinct class JSaxonApiException {

    *java:JObject;
    *JException;

    # The `handle` field that stores the reference to the `net.sf.saxon.s9api.SaxonApiException` object.
    public handle jObj;

    # The init function of the Ballerina class mapping the `net.sf.saxon.s9api.SaxonApiException` Java class.
    #
    # + obj - The `handle` value containing the Java reference of the object.
    public function init(handle obj) {
        self.jObj = obj;
    }

    # The function to retrieve the string representation of the Ballerina class mapping the `net.sf.saxon.s9api.SaxonApiException` Java class.
    #
    # + return - The `string` form of the Java object instance.
    public function toString() returns string {
        return java:toString(self.jObj) ?: "null";
    }
    # The function that maps to the `addSuppressed` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `Throwable` value required to map with the Java method parameter.
    public function addSuppressed(Throwable arg0) {
        net_sf_saxon_s9api_SaxonApiException_addSuppressed(self.jObj, arg0.jObj);
    }

    # The function that maps to the `equals` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `Object` value required to map with the Java method parameter.
    # + return - The `boolean` value returning from the Java mapping.
    public function 'equals(Object arg0) returns boolean {
        return net_sf_saxon_s9api_SaxonApiException_equals(self.jObj, arg0.jObj);
    }

    # The function that maps to the `fillInStackTrace` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `Throwable` value returning from the Java mapping.
    public function fillInStackTrace() returns Throwable {
        handle externalObj = net_sf_saxon_s9api_SaxonApiException_fillInStackTrace(self.jObj);
        Throwable newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getCause` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `Throwable` value returning from the Java mapping.
    public function getCause() returns Throwable {
        handle externalObj = net_sf_saxon_s9api_SaxonApiException_getCause(self.jObj);
        Throwable newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getClass` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `Class` value returning from the Java mapping.
    public function getClass() returns Class {
        handle externalObj = net_sf_saxon_s9api_SaxonApiException_getClass(self.jObj);
        Class newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getErrorCode` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `QName` value returning from the Java mapping.
    public function getErrorCode() returns QName {
        handle externalObj = net_sf_saxon_s9api_SaxonApiException_getErrorCode(self.jObj);
        QName newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `getLineNumber` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function getLineNumber() returns int {
        return net_sf_saxon_s9api_SaxonApiException_getLineNumber(self.jObj);
    }

    # The function that maps to the `getLocalizedMessage` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getLocalizedMessage() returns string? {
        return java:toString(net_sf_saxon_s9api_SaxonApiException_getLocalizedMessage(self.jObj));
    }

    # The function that maps to the `getMessage` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getMessage() returns string? {
        return java:toString(net_sf_saxon_s9api_SaxonApiException_getMessage(self.jObj));
    }

    # The function that maps to the `getStackTrace` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `StackTraceElement[]` value returning from the Java mapping.
    public function getStackTrace() returns StackTraceElement[]|error {
        handle externalObj = net_sf_saxon_s9api_SaxonApiException_getStackTrace(self.jObj);
        StackTraceElement[] newObj = [];
        handle[] anyObj = <handle[]>check jarrays:fromHandle(externalObj, "handle");
        int count = anyObj.length();
        foreach int i in 0 ... count - 1 {
            StackTraceElement element = new (anyObj[i]);
            newObj[i] = element;
        }
        return newObj;
    }

    # The function that maps to the `getSuppressed` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `Throwable[]` value returning from the Java mapping.
    public function getSuppressed() returns Throwable[]|error {
        handle externalObj = net_sf_saxon_s9api_SaxonApiException_getSuppressed(self.jObj);
        Throwable[] newObj = [];
        handle[] anyObj = <handle[]>check jarrays:fromHandle(externalObj, "handle");
        int count = anyObj.length();
        foreach int i in 0 ... count - 1 {
            Throwable element = new (anyObj[i]);
            newObj[i] = element;
        }
        return newObj;
    }

    # The function that maps to the `getSystemId` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `string` value returning from the Java mapping.
    public function getSystemId() returns string? {
        return java:toString(net_sf_saxon_s9api_SaxonApiException_getSystemId(self.jObj));
    }

    # The function that maps to the `hashCode` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `int` value returning from the Java mapping.
    public function hashCode() returns int {
        return net_sf_saxon_s9api_SaxonApiException_hashCode(self.jObj);
    }

    # The function that maps to the `initCause` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `Throwable` value required to map with the Java method parameter.
    # + return - The `Throwable` value returning from the Java mapping.
    public function initCause(Throwable arg0) returns Throwable {
        handle externalObj = net_sf_saxon_s9api_SaxonApiException_initCause(self.jObj, arg0.jObj);
        Throwable newObj = new (externalObj);
        return newObj;
    }

    # The function that maps to the `notify` method of `net.sf.saxon.s9api.SaxonApiException`.
    public function notify() {
        net_sf_saxon_s9api_SaxonApiException_notify(self.jObj);
    }

    # The function that maps to the `notifyAll` method of `net.sf.saxon.s9api.SaxonApiException`.
    public function notifyAll() {
        net_sf_saxon_s9api_SaxonApiException_notifyAll(self.jObj);
    }

    # The function that maps to the `printStackTrace` method of `net.sf.saxon.s9api.SaxonApiException`.
    public function printStackTrace() {
        net_sf_saxon_s9api_SaxonApiException_printStackTrace(self.jObj);
    }

    # The function that maps to the `printStackTrace` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `PrintStream` value required to map with the Java method parameter.
    public function printStackTrace2(PrintStream arg0) {
        net_sf_saxon_s9api_SaxonApiException_printStackTrace2(self.jObj, arg0.jObj);
    }

    # The function that maps to the `printStackTrace` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `PrintWriter` value required to map with the Java method parameter.
    public function printStackTrace3(PrintWriter arg0) {
        net_sf_saxon_s9api_SaxonApiException_printStackTrace3(self.jObj, arg0.jObj);
    }

    # The function that maps to the `setStackTrace` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `StackTraceElement[]` value required to map with the Java method parameter.
    # + return - The `error?` value returning from the Java mapping.
    public function setStackTrace(StackTraceElement[] arg0) returns error? {
        net_sf_saxon_s9api_SaxonApiException_setStackTrace(self.jObj, check jarrays:toHandle(arg0, "java.lang.StackTraceElement"));
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function 'wait() returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_SaxonApiException_wait(self.jObj);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait2(int arg0) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_SaxonApiException_wait2(self.jObj, arg0);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

    # The function that maps to the `wait` method of `net.sf.saxon.s9api.SaxonApiException`.
    #
    # + arg0 - The `int` value required to map with the Java method parameter.
    # + arg1 - The `int` value required to map with the Java method parameter.
    # + return - The `InterruptedException` value returning from the Java mapping.
    public function wait3(int arg0, int arg1) returns InterruptedException? {
        error|() externalObj = net_sf_saxon_s9api_SaxonApiException_wait3(self.jObj, arg0, arg1);
        if (externalObj is error) {
            InterruptedException e = error InterruptedException(INTERRUPTEDEXCEPTION, externalObj, message = externalObj.message());
            return e;
        }
    }

}

# The constructor function to generate an object of `net.sf.saxon.s9api.SaxonApiException`.
#
# + arg0 - The `string` value required to map with the Java constructor parameter.
# + return - The new `JSaxonApiException` class generated.
public function newJSaxonApiException1(string arg0) returns JSaxonApiException {
    handle externalObj = net_sf_saxon_s9api_SaxonApiException_newJSaxonApiException1(java:fromString(arg0));
    JSaxonApiException newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `net.sf.saxon.s9api.SaxonApiException`.
#
# + arg0 - The `string` value required to map with the Java constructor parameter.
# + arg1 - The `Throwable` value required to map with the Java constructor parameter.
# + return - The new `JSaxonApiException` class generated.
public function newJSaxonApiException2(string arg0, Throwable arg1) returns JSaxonApiException {
    handle externalObj = net_sf_saxon_s9api_SaxonApiException_newJSaxonApiException2(java:fromString(arg0), arg1.jObj);
    JSaxonApiException newObj = new (externalObj);
    return newObj;
}

# The constructor function to generate an object of `net.sf.saxon.s9api.SaxonApiException`.
#
# + arg0 - The `Throwable` value required to map with the Java constructor parameter.
# + return - The new `JSaxonApiException` class generated.
public function newJSaxonApiException3(Throwable arg0) returns JSaxonApiException {
    handle externalObj = net_sf_saxon_s9api_SaxonApiException_newJSaxonApiException3(arg0.jObj);
    JSaxonApiException newObj = new (externalObj);
    return newObj;
}

function net_sf_saxon_s9api_SaxonApiException_addSuppressed(handle receiver, handle arg0) = @java:Method {
    name: "addSuppressed",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.lang.Throwable"]
} external;

function net_sf_saxon_s9api_SaxonApiException_equals(handle receiver, handle arg0) returns boolean = @java:Method {
    name: "equals",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.lang.Object"]
} external;

function net_sf_saxon_s9api_SaxonApiException_fillInStackTrace(handle receiver) returns handle = @java:Method {
    name: "fillInStackTrace",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getCause(handle receiver) returns handle = @java:Method {
    name: "getCause",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getClass(handle receiver) returns handle = @java:Method {
    name: "getClass",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getErrorCode(handle receiver) returns handle = @java:Method {
    name: "getErrorCode",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getLineNumber(handle receiver) returns int = @java:Method {
    name: "getLineNumber",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getLocalizedMessage(handle receiver) returns handle = @java:Method {
    name: "getLocalizedMessage",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getMessage(handle receiver) returns handle = @java:Method {
    name: "getMessage",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getStackTrace(handle receiver) returns handle = @java:Method {
    name: "getStackTrace",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getSuppressed(handle receiver) returns handle = @java:Method {
    name: "getSuppressed",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_getSystemId(handle receiver) returns handle = @java:Method {
    name: "getSystemId",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_hashCode(handle receiver) returns int = @java:Method {
    name: "hashCode",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_initCause(handle receiver, handle arg0) returns handle = @java:Method {
    name: "initCause",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.lang.Throwable"]
} external;

function net_sf_saxon_s9api_SaxonApiException_notify(handle receiver) = @java:Method {
    name: "notify",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_notifyAll(handle receiver) = @java:Method {
    name: "notifyAll",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_printStackTrace(handle receiver) = @java:Method {
    name: "printStackTrace",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_printStackTrace2(handle receiver, handle arg0) = @java:Method {
    name: "printStackTrace",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.io.PrintStream"]
} external;

function net_sf_saxon_s9api_SaxonApiException_printStackTrace3(handle receiver, handle arg0) = @java:Method {
    name: "printStackTrace",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.io.PrintWriter"]
} external;

function net_sf_saxon_s9api_SaxonApiException_setStackTrace(handle receiver, handle arg0) = @java:Method {
    name: "setStackTrace",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["[Ljava.lang.StackTraceElement;"]
} external;

function net_sf_saxon_s9api_SaxonApiException_wait(handle receiver) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: []
} external;

function net_sf_saxon_s9api_SaxonApiException_wait2(handle receiver, int arg0) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["long"]
} external;

function net_sf_saxon_s9api_SaxonApiException_wait3(handle receiver, int arg0, int arg1) returns error? = @java:Method {
    name: "wait",
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["long", "int"]
} external;

function net_sf_saxon_s9api_SaxonApiException_newJSaxonApiException1(handle arg0) returns handle = @java:Constructor {
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.lang.String"]
} external;

function net_sf_saxon_s9api_SaxonApiException_newJSaxonApiException2(handle arg0, handle arg1) returns handle = @java:Constructor {
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.lang.String", "java.lang.Throwable"]
} external;

function net_sf_saxon_s9api_SaxonApiException_newJSaxonApiException3(handle arg0) returns handle = @java:Constructor {
    'class: "net.sf.saxon.s9api.SaxonApiException",
    paramTypes: ["java.lang.Throwable"]
} external;


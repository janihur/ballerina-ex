import ballerina/io;
import p001_saxon.saxon_api as saxon;

public function main(string stylesheetFilename, string inputFilename) returns error? {
    io:println("BEGIN");
    
    saxon:StreamSource stylesheet = check new(stylesheetFilename);
    saxon:Xslt xslt = check new(stylesheet);

    saxon:StreamSource input = check new(inputFilename);
    string output = check xslt.transform(input);
    
    io:println(output);
    
    io:println("END");
}

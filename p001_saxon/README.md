P001_SAXON
==========

Example about Java bindings and how to transform JSON with XSLT.

```
bal new p001_saxon
cd p001_saxon
bal add saxon_bindings
bal add saxon_api
```

```
bal bindgen -mvn net.sf.saxon:Saxon-HE:9.9.1-8 -o modules/saxon_bindings --public \
java.io.FileInputStream \
java.io.InputStream \
java.io.StringWriter \
javax.xml.transform.stream.StreamSource \
net.sf.saxon.s9api.Processor \
net.sf.saxon.s9api.SaxonApiException \
net.sf.saxon.s9api.Serializer \
net.sf.saxon.s9api.Xslt30Transformer \
net.sf.saxon.s9api.XsltCompiler \
net.sf.saxon.s9api.XsltExecutable
```

```
bal build
java -jar target/bin/p001_saxon.jar e032.xsl e032-input.json.xml
```

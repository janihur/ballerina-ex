// how to build a sexpr-like string
import ballerina/io;
public function main() {
    (List|Pair)[] stack = [];
    stack.push(<List>[LIST, "foobars"]);
    stack.push(<List>[BEGIN, "foos"]);
    stack.push(["foo", 1]);
    stack.push(["foo", 2]);
    stack.push(<List>[END, "foos"]);
    stack.push(<List>[LIST, "bars"]);
    stack.push(["bar", 3]);
    stack.push(["bar", 4]);

    final string s = stack.reverse().reduce(function(string accu, List|Pair item) returns string{
        return b(accu, item);
    }, "");

    io:println(s);
}

enum ListCons {
    LIST,
    BEGIN,
    END
}

type List [ListCons, string];
type Pair [string, any];

// builder
function b(string accu, List|Pair x) returns string {
    if x is List {
        [ListCons, string]['type, key] = x;
        match 'type {
            LIST =>  { return string`(${key} (${accu}))`; }
            BEGIN => { return string`(${key} (${accu}`; }
            END =>   { return string`))${accu}`; }
        }
    }

    string item = kv(x[0], x[1]);
    return string`${item}${accu}`;
}

// keyvalue
function kv(string k, any v) returns string {
    string value;
    if v is string {
        value = string`"${v}"`;
    } else if v is () {
        value = "nil";
    } else {
        value =  v.toString();
    }
    return string`(${k} ${value})`;
}

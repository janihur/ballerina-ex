import ballerina/io;

public type IntResult record {|
    int value;
|};


public class IntIter {
    int c;
    final int end;
    final int step;
    final function(int, int) returns boolean stopFn;

    function init(int beg, int end, int? step = ()) {
        self.c = beg;
        self.end = end;
        if beg < end { // increment
            self.step = step ?: 1;
            //self.cmp = function (int beg_, int end_) returns boolean { return beg_ >= end_; };
            self.stopFn = (curr, term) => curr >= term;
        } else if beg > end { // decrement
            self.step = step ?: -1;
            //self.stopFn = function (int beg_, int end_) returns boolean { return beg_ <= end_; };
            self.stopFn = (curr, term) => curr <= term;
        } else { // quit
            self.step = 0;
            //self.stopFn = function (int beg_, int end_) returns boolean { return false; };
            self.stopFn = (curr, term) => true;
        }
    }

    // Implements the `next` method
    public function next() returns IntResult? {
        if self.stopFn(self.c, self.end) {
            return ();
        }
        IntResult r = {value: self.c};
        self.c += self.step;
        return r;
    }
}

public class IntIterable {
    // Need to have this type inclusion as to indicate the distinct type relationship
    *object:Iterable;

    final int beg;
    final int end;
    final int? step;

    function init(int beg, int end, int? step = ()) {
        self.beg = beg;
        self.end = end;
        self.step = step;
    }
    
    public function iterator() returns IntIter {
        return new IntIter(self.beg, self.end, self.step);
    }
}

function intRange(int beg, int end, int? step = ()) returns IntIterable {
    return new IntIterable(beg, end, step);
}
public function main() {
    foreach int i in intRange(3,9, 2) {
        io:print(i);
    }
    io:println();

    foreach int i in intRange(9,3, -2) {
        io:print(i);
    }
    io:println();

    foreach int i in intRange(3,3) {
        io:print(i);
    }
    io:println();
}
import ballerina/io;

// ----------------------------------------------------------------------------
// #1) one-way one-to-many relation
// one Foo can have only one Bar but Bar can have any number of Foos
// you can only navigate from Foo to Bar
type Foo1M record {|
    readonly int id;
    string data;
    int barId;
|};
type Foo1Ms table<Foo1M> key(id);

type Bar1M record {|
    readonly int id;
    string data;
|};
type Bar1Ms table<Bar1M> key(id);

// ----------------------------------------------------------------------------
// #2) two-way many-to-many relation with separate associative table
// navigation possible in both directions
// https://en.wikipedia.org/wiki/Associative_entity
// NOTE: the join is always an inner join
type FooMM record {|
    readonly int id;
    string data;
|};
type FooMMs table<FooMM> key(id);

type BarMM record {|
    readonly int id;
    string data;
|};
type BarMMs table<BarMM> key(id);

type FooBarAssociation record {|
    readonly int fooId;
    readonly int barId;
|};
type FooBarAssociations table<FooBarAssociation> key(fooId, barId);

// ----------------------------------------------------------------------------
// #3) one-way many-to-many relation
// you can only navigate from Foo to Bar
type FooMM2 record {|
    readonly int id;
    string data;
    int[] barIds;
|};
type FooMM2s table<FooMM2> key(id);

type BarMM2 record {|
    readonly int id;
    string data;
|};
type BarMM2s table<BarMM2> key(id);

public function main() {
    do {
        Foo1Ms foos = table [
            {id: 1, data: "foo 1", barId: 2},
            {id: 3, data: "foo 3", barId: 4},
            {id: 5, data: "foo 5", barId: 4},
            {id: 7, data: "foo 7", barId: 6}
        ];
        Bar1Ms bars = table [
            {id: 2, data: "bar 2"},
            {id: 4, data: "bar 4"},
            {id: 6, data: "bar 6"}
        ];

        record {|int id; string data;|}[] foosWithBarsData =
            from var foo in foos
            where foo.barId == 4
            join var bar in bars on foo.barId equals bar.id
            select {
                id: foo.id,
                data: bar.data
            }
        ;

        io:println(foosWithBarsData);
    }

    do {
        FooMMs foos = table [
            {id: 1, data: "foo 1"},
            {id: 3, data: "foo 3"},
            {id: 5, data: "foo 5"}
        ];

        BarMMs bars = table [
            {id: 2, data: "bar 2"},
            {id: 4, data: "bar 4"},
            {id: 6, data: "bar 6"}
        ];

        FooBarAssociations fooBarRelations = table [
            {fooId: 1, barId: 2},
            {fooId: 1, barId: 4},
            {fooId: 3, barId: 4},
            {fooId: 3, barId: 6},
            {fooId: 5, barId: 6}
        ];

        record {|int fooId; int barId; string data;|}[] fooBars = 
            from var relation in fooBarRelations
            join var foo in foos on relation.fooId equals foo.id
            join var bar in bars on relation.barId equals bar.id
            select {
                fooId: relation.fooId,
                barId: relation.barId,
                data: string`${foo.data};${bar.data}`
            }
        ;

        io:println(fooBars);
    }

    do {
        FooMM2s foos = table [
            {id: 1, data: "foo 1", barIds: [2, 4]},
            {id: 3, data: "foo 3", barIds: [4, 6]},
            {id: 5, data: "foo 5", barIds: [6]}
        ];

        BarMM2s bars = table [
            {id: 2, data: "bar 2"},
            {id: 4, data: "bar 4"},
            {id: 6, data: "bar 6"}
        ];

        // TODO is simpler way possible?
        table<record {|int fooId; int barId; string data;|}> fooBars = table [];

        foreach var foo in foos {
            table <record {|int id;|}> barIds = table [];
            foreach var barId in foo.barIds {
                barIds.add({id: barId});
            }

            var temps =
                from var barId in barIds
                join var bar in bars on barId.id equals bar.id
                select {
                    fooId: foo.id,
                    barId: barId.id,
                    data: string`${foo.data};${bar.data}`
                }
            ;

            foreach var temp in temps {
                fooBars.add(temp);
            }   
        }
 
        io:println(fooBars);
    }
}
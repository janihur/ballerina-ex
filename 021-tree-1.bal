import ballerina/io;

type Node record {|
    string value;
    Node[] children = [];
|};

int level = -1;

function printer(string value) {
    foreach int i in 1...level {
        io:print(" ");
    }
    io:println(value);
}

function walkdown(Node node) {
    level += 1;
    printer(node.value);
    foreach var child in node.children {
        walkdown(child);
    }
    level -= 1;
}

public function main() {
    Node root = {
        value: "A",
        children: []
    };
    root.children.push({ value: "B" }, { value: "C" });
    root.children[0].children.push({ value: "D"});
    root.children[1].children.push({ value: "E", children: [{ value: "F" }]});

    walkdown(root);
}
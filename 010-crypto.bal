import ballerina/crypto;
import ballerina/io;
import ballerina/lang.array;
import ballerina/lang.'string;

string secret = "I'm a secret - tell me no-one !";
string key = "this 32-bytes long key unlock th"; // 32 bytes
public function main() returns error? {
    io:println("case #1 -----");


    io:println(string`original non-encrypted secret: ${secret}`);

    byte[] secretBytes = secret.toBytes();
    byte[] keyBytes = key.toBytes();

    byte[] cipherText = check crypto:encryptAesEcb(secretBytes, keyBytes);

    io:println(string`encrypted secret as Base64: ${array:toBase64(cipherText)}`);

    byte[] plainText = check crypto:decryptAesEcb(cipherText, keyBytes);

    io:println(plainText);
    io:println(string`decrypted secret: ${check string:fromCodePointInts(plainText)}`);

    io:println("case #2 -----");
    {
        string encryptedText = check encrypt1(secret, key);
        io:println(string`encrypted: ${encryptedText}`);
        io:println(string`decrypted: ${check decrypt1(encryptedText, key)}`);
    }

}

function encrypt1(string secret, string key) returns string|error {
    byte[] secretBytes = secret.toBytes();
    byte[] keyBytes = key.toBytes();

    byte[] cipherText = check crypto:encryptAesEcb(secretBytes, keyBytes);
    
    return array:toBase64(cipherText);
}

function decrypt1(string secret, string key) returns string|error {
    byte[] cipherText = check array:fromBase64(secret);
    byte[] keyBytes = key.toBytes();

    byte[] plainText = check crypto:decryptAesEcb(cipherText, keyBytes);

    return check string:fromCodePointInts(plainText);
}
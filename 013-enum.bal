import ballerina/io;
public function main() {
    io:println(isValidColor("yellow"));
    io:println(isValidColor("blue"));
    io:println(isValidColor(YELLOW));
    io:println(isValidColor(BLUE));
    io:println(isValidColor("red"));
}

function isValidColor(string color) returns boolean {
    return color is ValidColor;
}

enum ValidColor {
    YELLOW = "yellow",
    BLUE = "blue"
}
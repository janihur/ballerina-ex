import ballerina/io;
public function main() {
    // int? is just sugar for int|() union type
    // () is nil, i.e. type for "no-value"
    int? b = ();

    if b is int {
        io:println(string`(b ${b})`);
    } else {
        io:println("(b nil)");
    }

    b = 1;
    
    if b is int {
        io:println(string`(b ${b})`);
    } else {
        io:println("(b nil)");
    }
}

import ballerina/io;
public function main() returns error? {
    json[] input = [{foo: "one"}, {foo: "two"}, {foo: "three"}];
    
    foreach var item in input {
        io:print(item);
        string x = check item.foo;
        io:println(" " + x);
    }

    json[] output = from var item in input
        let string val = check item.foo
        where val != "two"
        select {
            bar: val
        }
    ;

    io:println(output);
}

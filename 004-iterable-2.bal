import ballerina/io;

public type Result record {|
    int value;
|};

public class InclusiveIter {
    int c;
    final int max;

    function init(int max) {
        self.max = max;
        self.c = 0;
    }

    // Implements the `next` method
    public function next() returns Result? {
        if self.c > self.max {
            return ();
        }
        Result r = {value: self.c};
        self.c += 1;
        return r;
    }
}

public class Iterable {
    // Need to have this type inclusion as to indicate the distinct type relationship
    *object:Iterable;

    final int max;

    function init(int max) {
        self.max = max;
    }
    public function iterator() returns InclusiveIter {
        return new InclusiveIter(self.max);
    }
}

public function main() {
    Iterable iter = new Iterable(10);
    foreach var i in iter {
        io:println(i);
    }
}
import ballerina/io;

public class Y {
    int? y;
    public function init(int? y) {
        self.y = y;
    }
    public function print() {
        if self.y is () { // "if type test"
            io:println("nil");
        } else {
            // "if type test" narrows only local variables and parameters :(
            // so here we have to do it explicitly
            var tmp = <int>self.y;
            if tmp > 0 {
                io:println("positive");
            } else {
                io:println("whatever");
            }
        }
    }
}
public function main() {
    int? x = ();
    if x is () { // "if type test"
        io:println("nil");
    } else {
        // "if type test" narrows the variable as expected
        if x > 0 {
            io:println("positive");
        } else {
            io:println("whatever");
        }
    }

    Y y1 = new(());
    y1.print();

    Y y2 = new(1);
    y2.print();
}
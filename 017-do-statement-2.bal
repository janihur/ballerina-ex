import ballerina/io;
public function main() {
    do { // i is int
        int? i = check f("42");
        io:println(string`(i ${i ?: "nil"})`);
    } on fail error err {
        io:println(err);
    }

    do { // i is nil, i.e. there is no value
        var i = check f("43"); // type interference
        io:println(string`(i ${i ?: "nil"})`);
    } on fail error err {
        io:println(err);
    }

    do { // error!
        int? i = check f("skrolli");
        io:println(string`(i ${i ?: "nil"})`);
    } on fail error err {
        io:println(err);
    }
}

function f(string s) returns int?|error {
    int i = check int:fromString(s);
    return g(i);
}

function g(int i) returns int? {
    if i != 42 {
        return ();
    } else {
        return i;
    }
}
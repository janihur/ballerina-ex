import ballerina/http;
import ballerina/io;
import ballerina/log;
import ballerina/mime;

function saveToPath(string path, xml x) returns error? {
    return io:fileWriteXml(path, x);
}

function createResponse() returns http:Response {
    xml XHTMLResponse = xml `<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
    <meta http-equiv="refresh" content="6;URL='/upload'" /> 
</head>     
<body>     
<div class="container">
<h3>Upload successful:</h3>
<div class="row">
    <div class="file-field input-field  col s12">
      The files were read successfully and were sent on for checking and processing. You will
      be returned to the uploader in a few seconds...
    </div>
</div>
</div>
</body>
</html>`;

    http:Response response = new;
    response.setPayload(XHTMLResponse);
    return response;
}

service / on new http:Listener(9092, {requestLimits:{maxEntityBodySize: 1024},timeout:500}) {
    resource function post input2(http:Request request) returns http:Response|error {
        
        mime:Entity body = (check request.getBodyParts())[0];

        if body.getContentType() != mime:TEXT_XML {
            return error("INVALID_CONTENT_TYPE");
        }
    
        xml x = check body.getXml();
        string id = check x.<foo>.id;

        string path = "";
        match id {
            "foo" => {
                path = "foofoo.xml";
            }
            "bar" => {
                path = "foobar.xml";
            }
            _ => {
                path = "unknown.xml";
            }
        }

        check io:fileWriteXml(path, x);

        return createResponse();
    }

    resource function post input(http:Request request) returns http:Response|error {
        mime:Entity[] bodyParts = check request.getBodyParts();
        int counter = 0;
        foreach mime:Entity bodyPart in bodyParts {
            var mediaType = mime:getMediaType(bodyPart.getContentType());
            if (mediaType is mime:MediaType) {
                string baseType = mediaType.getBaseType();
                log:printInfo(baseType.toString());
                match baseType {
                    "text/xml" => {
                        log:printInfo("MIME: text/xml");
                        stream<byte[], io:Error?> inputStream = check bodyPart.getByteStream();
                        xml inputStreamXML = xml `${inputStream.toBalString()}`;
                        string|error Id = inputStreamXML.<div>.id;
                        match Id {
                                "XXX" => {
                                    log:printInfo("Content match: id=XXX");
                                    check io:fileWriteBlocksFromStream(string `./uploads/file_${counter}.html`, inputStream);
                                }
                                _ => {
                                    log:printInfo("Content match: something else");
                                }
                            }
                    }
                    "text/plain" => {
                        log:printInfo("MIME: text/plain");
                    }
                    "none" => {
                        log:printInfo("MIME: none");
                     }
                     _ => {
                        log:printInfo("MIME: something else");
                     }
                }
             }      
            counter += 1;
        }

        http:Response response = new;
    xml XHTMLResponse = xml `<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
    <meta http-equiv="refresh" content="6;URL='/upload'" /> 
</head>     
<body>     
<div class="container">
<h3>Upload successful:</h3>
<div class="row">
    <div class="file-field input-field col s12">
      The files were read successfully and were sent on for checking and processing. You will
      be returned to the uploader in a few seconds...
    </div>
</div>
</div>
</body>
</html>`;
        response.setPayload(XHTMLResponse);
        return response;
    }

    resource function get upload() returns http:Response|error {
        http:Response r = new;
        r.setFileAsPayload("018-file-upload-upload.html", mime:TEXT_HTML);
        return r;
    }
}
P003_DOCKER
===========

Simple Docker example derived from Ballerina documentation: [Code to Cloud](https://ballerina.io/learn/run-ballerina-programs-in-the-cloud/code-to-cloud-deployment/#docker-deployment).


Standalone
----------
```
bal run
```
```
curl http://localhost:9090/hello
curl http://localhost:9090/hello?name=jani
```

With Docker
-----------
Note the example has been configured ([`Cloud.toml`](Cloud.toml)) to build `Dockerfile` only. The Docker image have to be build in a separate step.
```
# creates the Dockerfile
bal build --cloud=docker
# creates the Docker image (Lubuntu requires sudo with Docker commands)
sudo docker build target/docker/p003_docker/
```

```
# find out <IMAGE_ID>
$ sudo docker images
REPOSITORY        TAG       IMAGE ID       CREATED              SIZE
<none>            <none>    77e23280f691   About a minute ago   217MB
ballerina/jre11   v1        061ac3893820   8 months ago         186MB
#
$ sudo docker run --detach --publish 9090:9090 --name p003_docker <IMAGE_ID>
<CONTAINER_ID>
# check the logs (but there's nothing at the moment)
$ sudo docker logs <CONTAINER_ID>
```

// import ballerina/http;

// listener http:Listener ep0 = new (8080, config = {host: "localhost"});

// service / on ep0 {
//     resource function get hello(string? name) returns json {

//         return { hello: name ?: "unknown" };
//     }
// }

 import ballerina/http;

 //listener http:Listener ep = new(9090);

 service / on new http:Listener(9090) {
    resource function get hello(string? name) returns json {
        return { hello: name ?: "unknown" };
    }
 }